	<?php
	/*
	PRODUCED BY:lnwPHP Thailand (lnwPHP Admin Manager)
	AUTHOR:Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	COPYRIGHT 2014 ALL RIGHTS RESERVED

	You must have purchased a valid license from lnwPHP.in.th in order to have 
	access this file.

	You may only use this file according to the respective licensing terms 
	you agreed to when purchasing this item.
	*/
	$haccess = new admin_users_model();
	//$dbc->logged_in_protect();
	$errors = array();

	if (empty($_POST) === false) {

	$username = trim(post('username'));
	$password = trim(post('password'));

	if (empty($username) === true || empty($password) === true) {
		$errors[] = LANG_ADMIN_INVALID_PASSWORD;
	} else if ($haccess->user_exist_checker($username,H_SYSTEM_ACCESS) === false) {
		$errors[] = LANG_ADMIN_INVALID_USERNAME;
	} else if ($haccess->account_activation($username,H_SYSTEM_ACCESS) === false) {
		$errors[] = LANG_ADMIN_INVALID_ACTIVATION;
	} else {
		
		$login = $haccess->HezeLogin($username, $password,H_SYSTEM_ACCESS);
		if ($login === false) {
			$errors[] = LANG_ADMIN_ERROR;
		}else {
			$_SESSION[H_USER_SESSION] =  $login;
			$haccess->UpdateLastLogin(date('Y-m-d'),$_SERVER['REMOTE_ADDR'],$haccess->UserID());
			send_to(''.H_ADMIN.'&view=hsys_users&do=viewall');
			exit();
		}
	}
} 
?>


    <div class="col-lg-6">
	
    <form method="post" action="<?php htmlspecialchars($_SERVER['PHP_SELF']);?>" name="admin" class="form-signin" role="form" id="hezecomform">
    <div class="form-group">
	<h2 class="form-signin-heading"><?php echo LANG_ADMIN_LOGIN;?></h2>
    <?php form_errors($errors)?>
    </div>
    
    <div class="form-group">
    <input name="username" type="text" required class="form-control"  placeholder="<?php echo LANG_ADMIN_USERNAME;?>" maxlength="20">
    </div>
    <div class="form-group">
    <input type="password" name="password" class="form-control"  placeholder="<?php echo LANG_ADMIN_PASSWORD;?>" required>
    </div>
    
      <div class="controls">
	 <div class="col-md-8" style="padding:0;">
	 <input name="Logme" class="btn btn-primary btn-lg " type="submit" value="<?php echo LANG_ADMIN_BUTTON;?>">
	 </div>
	 <div class="col-md-4" style="padding:0;">
	 <div id="output"></div>
	 </div>
	 </div>

    </form>
    
    </div>
