<?php
	/*
	PRODUCED BY:lnwPHP Thailand (lnwPHP Admin Manager)
	AUTHOR:Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	COPYRIGHT 2014 ALL RIGHTS RESERVED

	You must have purchased a valid license from lnwPHP.in.th in order to have 
	access this file.

	You may only use this file according to the respective licensing terms 
	you agreed to when purchasing this item.
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
class DB{
private static $instance = NULL;
private function __construct() {
}
public static function getInstance() {

if (!self::$instance)
    {
    self::$instance = new PDO("".DB_TYPE.":host=".LOCALHOST.";dbname=".DB_NAME."", DB_USERNAME, DB_PASSWORD);
    self::$instance-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
return self::$instance;
}
private function __clone(){
}
}

	class dboptions{
	//select all
		public function rawSelect($sql)
        {
        $db=DB::getInstance();
		try{
			return $db->query($sql);
			} catch (PDOException $e){
			die($e->getMessage());
		}
        }

      //insert
		public function dbInsert($table, $values)
        {
            $db=DB::getInstance();
            $fieldnames = array_keys($values[0]);
            $size = sizeof($fieldnames);
            $i = 1;
            $sql = "INSERT INTO $table";
            $fields = '( ' . implode(' ,', $fieldnames) . ' )';
            $bound = '(:' . implode(', :', $fieldnames) . ' )';
            $sql .= $fields.' VALUES '.$bound;
            $stmt = $db->prepare($sql);
            foreach($values as $vals)
            {
                $stmt->execute($vals);
            }
        }
	//delete
        public function dbDelete($table, $fieldname, $id)
        {
            $db=DB::getInstance();
            $sql = "DELETE FROM `$table` WHERE `$fieldname` = :id";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_STR);
			try{
			$stmt->execute();
			} catch (PDOException $e){
			die($e->getMessage());
			}
        }
		//SELECT COUNT FOR PAGINATION
		public function SelectCount($where)
		{
		$record = $this->rawSelect ("$where");
		$rowcount= $record->fetch(PDO::FETCH_OBJ);
		return $rowcount->num;
		}
		
		
}

?>