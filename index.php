<?php
	/*
	PRODUCED BY:lnwPHP Thailand (lnwPHP Admin Manager)
	AUTHOR:Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	COPYRIGHT 2014 ALL RIGHTS RESERVED

	You must have purchased a valid license from lnwPHP.in.th in order to have 
	access this file.

	You may only use this file according to the respective licensing terms 
	you agreed to when purchasing this item.
	*/

define('bz_lnwphp', true);

require_once 'config.php';

require_once 'libs/function/class.DBPDO.php';

require_once 'libs/Smarty.class.php';

require_once 'system/header.php';

if (isset($_GET['page'])) {
	require_once 'system/'.$_GET['page'].'.php';
}elseif(isset($_GET['search'])){
	require_once 'system/search.php';
}else{
	require_once 'system/main.php';
}

require_once 'system/footer.php';
?>