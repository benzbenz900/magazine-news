<?php

	/*
	PRODUCED BY:lnwPHP Thailand (lnwPHP Admin Manager)
	AUTHOR:Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	COPYRIGHT 2014 ALL RIGHTS RESERVED
	Configuration File
	FILE NAME config.php 
	
	You must have purchased a valid license from lnwPHP.in.th in order to have 
	access this file.

	You may only use this file according to the respective licensing terms 
	you agreed to when purchasing this item.
	*/
	

$path=dirname(__FILE__);  
$npath = str_replace('\\', '/', $path); 
$npath= str_replace($_SERVER['DOCUMENT_ROOT'], '', $npath); 
$absolutep= str_replace('config','',$npath);

require_once './config.php';

define('URL_WEB',LP_TEMPLATES);

define('LOCALHOST',DATABASE_HOST);
define('DB_USERNAME',DATABASE_USER);
define('DB_PASSWORD',DATABASE_PASS);
define('DB_NAME',DATABASE_NAME);

define('DB_TYPE','mysql');
define('H_TITLE','lnwPHP Admin');

define('PAGINATION_TYPE','Normal');

define('RECORD_PER_PAGE','20');

define('BIG_IMAGE_WIDTH','600');
define('THUMB_IMAGE_WIDTH','150');

//Admin Login Info
define('ADMIN_USERNAME','admin');
define('ADMIN_PASSWORD','lnwphp');

//Upload Directory
define('UPLOAD_FOLDER','public/uploads/');
define('THUMB_FOLDER','public/uploads/thumbs/');
$base = str_replace('config','',dirname(__FILE__).DIRECTORY_SEPARATOR.'/');
define('UPLOAD_PATH',$base.UPLOAD_FOLDER);
define('THUMB_PATH',$base.THUMB_FOLDER);

define('VALID_DIR',1);
define('APP_PATH',$base);
define('APP_FOLDER',APP_PATH.'application');
define('DEFAULT_THEME','public/themes/default');
define('H_THEME',DEFAULT_THEME);
define('H_ADMIN','admin.php?pg=admin');
define('H_ADMIN_MAIN','main.php?pg=admin');

//Backup Dir 
define('UPLOAD_TABLE','hfiles');
define('FILE_ID','fid');
define('RELATE_ID','relateid');
define('H_FILE','gfile');
define('H_DATE','gdate');

//System Access
define('H_SYSTEM_ACCESS','users');
define('H_USER_SESSION','users');
?>