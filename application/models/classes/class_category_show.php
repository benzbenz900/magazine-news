
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        category_show
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		category_show
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* IMPORTANT:		
	* 'post()' is a defined function located @ lib/funtions.php
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	//Begin class
	
	class category_show
	{
	public $id;
	public $category; 
	public $categoryname; 
	public $category_show; 
	
	//Constructor
	public function __construct()
	{
	$this->id = isset($id);
	$this->category = isset($category);
	$this->categoryname = isset($categoryname);
	$this->category_show = isset($category_show);
	}
	}