
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        page_menu
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		page_menu
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* IMPORTANT:		
	* 'post()' is a defined function located @ lib/funtions.php
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	//Begin class
	
	class page_menu
	{
	public $id;
	public $name; 
	public $show_lp; 
	public $detail; 
	public $link; 
	public $keyword; 
	public $descirption; 
	public $order_lp; 
	
	//Constructor
	public function __construct()
	{
	$this->id = isset($id);
	$this->name = isset($name);
	$this->show_lp = isset($show_lp);
	$this->detail = isset($detail);
	$this->link = isset($link);
	$this->keyword = isset($keyword);
	$this->descirption = isset($descirption);
	$this->order_lp = isset($order_lp);
	}
	}