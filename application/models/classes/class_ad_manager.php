
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        ad_manager
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		ad_manager
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* IMPORTANT:		
	* 'post()' is a defined function located @ lib/funtions.php
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	//Begin class
	
	class ad_manager
	{
	public $id;
	public $name_ad; 
	public $codelink; 
	public $image; 
	public $show_lp; 
	public $end_date; 
	public $position; 
	
	//Constructor
	public function __construct()
	{
	$this->id = isset($id);
	$this->name_ad = isset($name_ad);
	$this->codelink = isset($codelink);
	$this->image = isset($image);
	$this->show_lp = isset($show_lp);
	$this->end_date = isset($end_date);
	$this->position = isset($position);
	}
	}