
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        category
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		category
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* IMPORTANT:		
	* 'post()' is a defined function located @ lib/funtions.php
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	//Begin class
	
	class category
	{
	public $id;
	public $name; 
	public $detail; 
	public $descirption; 
	public $keyword; 
	
	//Constructor
	public function __construct()
	{
	$this->id = isset($id);
	$this->name = isset($name);
	$this->detail = isset($detail);
	$this->descirption = isset($descirption);
	$this->keyword = isset($keyword);
	}
	}