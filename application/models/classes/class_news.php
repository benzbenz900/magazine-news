
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        news
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		news
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* IMPORTANT:		
	* 'post()' is a defined function located @ lib/funtions.php
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	//Begin class
	
	class news
	{
	public $id;
	public $image; 
	public $name; 
	public $type; 
	public $type2; 
	public $type3; 
	public $type4; 
	public $type5; 
	public $detail; 
	public $date_add; 
	public $date_update; 
	public $view; 
	public $addby; 
	public $label; 
	
	//Constructor
	public function __construct()
	{
	$this->id = isset($id);
	$this->image = isset($image);
	$this->name = isset($name);
	$this->type = isset($type);
	$this->type2 = isset($type2);
	$this->type3 = isset($type3);
	$this->type4 = isset($type4);
	$this->type5 = isset($type5);
	$this->detail = isset($detail);
	$this->date_add = isset($date_add);
	$this->date_update = isset($date_update);
	$this->view = isset($view);
	$this->addby = isset($addby);
	$this->label = isset($label);
	}
	}