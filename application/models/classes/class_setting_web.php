
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        setting_web
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		setting_web
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* IMPORTANT:		
	* 'post()' is a defined function located @ lib/funtions.php
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	//Begin class
	
	class setting_web
	{
	public $id;
	public $name_web; 
	public $decription; 
	public $keyword; 
	public $logo_web; 
	public $open_web; 
	public $contact_us; 
	
	//Constructor
	public function __construct()
	{
	$this->id = isset($id);
	$this->name_web = isset($name_web);
	$this->decription = isset($decription);
	$this->keyword = isset($keyword);
	$this->logo_web = isset($logo_web);
	$this->open_web = isset($open_web);
	$this->contact_us = isset($contact_us);
	}
	}