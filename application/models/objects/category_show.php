
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        category_show_model
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		category_show
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include_once(APP_FOLDER.'/models/classes/class_category_show.php');
	
	class category_show_model{
	
	// SELECT ALL
	public function SelectAll($limit=null)
	{
	$dbc = new dboptions();
	if($limit){
	$startpg = pageparam($limit);
	$record = $dbc->rawSelect ("SELECT * FROM category_show LIMIT {$startpg} , {$limit}");
	}else{
	$record = $dbc->rawSelect ("SELECT * FROM category_show");	
	}
	return $record->fetchAll(PDO::FETCH_OBJ);
	}
	
	//Select Count for Pagination
	public function CountRow()
	{
	$dbc = new dboptions();
	return $dbc->SelectCount('SELECT COUNT(*) as num FROM category_show');
	}
	
	// SELECT ONE
	public function SelectOne($id)
	{
	$db=DB::getInstance();
    $sql = "SELECT * FROM category_show WHERE id=:id";
	$stmt=$db->prepare($sql);
	$stmt->bindParam(':id',$id, PDO::PARAM_INT);
	$stmt->execute();
	return $stmt->fetch(PDO::FETCH_OBJ);
	}
	
	// TRUNCATE TABLE
	public function TruncateTable($redirect_to)
	{
	$db=DB::getInstance();
   	$sql=$db->prepare("TRUNCATE category_show");
	$sql->execute();
	send_to($redirect_to);
	}
	
	// DELETE
	public function Delete($id,$redirect_to)
	{
	$dbc = new dboptions();
	$dbc->dbDelete('category_show', 'id',$id);
	send_to($redirect_to);
	}
	
	// INSERT
	public function Insert($category,$categoryname,$category_show)
	{
	$dbc=new dboptions();

	
	$values = array(array( 'category'=>$category,'categoryname'=>$categoryname,'category_show'=>$category_show ));
	$dbc->dbInsert('category_show', $values);
	}
	
	// UPDATE
	public function Update($category,$categoryname,$category_show,$id)
	{
	$db=DB::getInstance();
	$sql = " UPDATE category_show SET  category =:category,categoryname =:categoryname,category_show =:category_show WHERE id = :id ";
	$data = array(':category'=>$category,':categoryname'=>$categoryname,':category_show'=>$category_show,':id'=>$id);
	$stmt=$db->prepare($sql);
	$stmt->execute($data);
	
	}
	
	
	} // end class
	
	?>
	
	