
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        ad_manager_model
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		ad_manager
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include_once(APP_FOLDER.'/models/classes/class_ad_manager.php');
	
	class ad_manager_model{
	
	// SELECT ALL
	public function SelectAll($limit=null)
	{
	$dbc = new dboptions();
	if($limit){
	$startpg = pageparam($limit);
	$record = $dbc->rawSelect ("SELECT * FROM ad_manager LIMIT {$startpg} , {$limit}");
	}else{
	$record = $dbc->rawSelect ("SELECT * FROM ad_manager");	
	}
	return $record->fetchAll(PDO::FETCH_OBJ);
	}
	
	//Select Count for Pagination
	public function CountRow()
	{
	$dbc = new dboptions();
	return $dbc->SelectCount('SELECT COUNT(*) as num FROM ad_manager');
	}
	
	// SELECT ONE
	public function SelectOne($id)
	{
	$db=DB::getInstance();
    $sql = "SELECT * FROM ad_manager WHERE id=:id";
	$stmt=$db->prepare($sql);
	$stmt->bindParam(':id',$id, PDO::PARAM_INT);
	$stmt->execute();
	return $stmt->fetch(PDO::FETCH_OBJ);
	}
	
	// TRUNCATE TABLE
	public function TruncateTable($redirect_to)
	{
	$db=DB::getInstance();
   	$sql=$db->prepare("TRUNCATE ad_manager");
	$sql->execute();
	send_to($redirect_to);
	}
	
	// DELETE
	public function Delete($id,$redirect_to)
	{
	$dbc = new dboptions();
	$dbc->dbDelete('ad_manager', 'id',$id);
	send_to($redirect_to);
	}
	
	// INSERT
	public function Insert($name_ad,$codelink,$show_lp,$end_date,$position)
	{
	$dbc=new dboptions();

	$newupload = new UploadControl;
	$uploadname=$newupload->ImageUplaodResize('image',THUMB_IMAGE_WIDTH,BIG_IMAGE_WIDTH,UPLOAD_PATH,THUMB_PATH,90);
	if($uploadname==''){
	$values = array(array( 'name_ad'=>$name_ad,'codelink'=>$codelink,'show_lp'=>$show_lp,'end_date'=>$end_date,'position'=>$position ));
	}else{
	$values = array(array( 'image'=>$uploadname,'name_ad'=>$name_ad,'codelink'=>$codelink,'show_lp'=>$show_lp,'end_date'=>$end_date,'position'=>$position ));
	}
	$dbc->dbInsert('ad_manager', $values);
	}
	
	// UPDATE
	public function Update($name_ad,$codelink,$show_lp,$end_date,$position,$id)
	{
	$db=DB::getInstance();
	$newupload = new UploadControl;
	$uploadname=$newupload->ImageUplaodResize('image',THUMB_IMAGE_WIDTH,BIG_IMAGE_WIDTH,UPLOAD_PATH,THUMB_PATH,90);
	if($uploadname==''){
	$sql = " UPDATE ad_manager SET  name_ad =:name_ad,codelink =:codelink,show_lp =:show_lp,end_date =:end_date,position =:position WHERE id = :id ";
	$data = array(':name_ad'=>$name_ad,':codelink'=>$codelink,':show_lp'=>$show_lp,':end_date'=>$end_date,':position'=>$position,':id'=>$id);
	}else{
	$sql = " UPDATE ad_manager SET  image=:image,name_ad =:name_ad,codelink =:codelink,show_lp =:show_lp,end_date =:end_date,position =:position WHERE id = :id ";
	$data = array(':image'=>$uploadname,':name_ad'=>$name_ad,':codelink'=>$codelink,':show_lp'=>$show_lp,':end_date'=>$end_date,':position'=>$position,':id'=>$id);
	}
	$stmt=$db->prepare($sql);
	$stmt->execute($data);
	
	}
	
	
	} // end class
	
	?>
	
	