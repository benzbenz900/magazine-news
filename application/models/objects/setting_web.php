
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        setting_web_model
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		setting_web
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include_once(APP_FOLDER.'/models/classes/class_setting_web.php');
	
	class setting_web_model{
	
	// SELECT ALL
	public function SelectAll($limit=null)
	{
	$dbc = new dboptions();
	if($limit){
	$startpg = pageparam($limit);
	$record = $dbc->rawSelect ("SELECT * FROM setting_web LIMIT {$startpg} , {$limit}");
	}else{
	$record = $dbc->rawSelect ("SELECT * FROM setting_web");	
	}
	return $record->fetchAll(PDO::FETCH_OBJ);
	}
	
	//Select Count for Pagination
	public function CountRow()
	{
	$dbc = new dboptions();
	return $dbc->SelectCount('SELECT COUNT(*) as num FROM setting_web');
	}
	
	// SELECT ONE
	public function SelectOne($id)
	{
	$db=DB::getInstance();
    $sql = "SELECT * FROM setting_web WHERE id=:id";
	$stmt=$db->prepare($sql);
	$stmt->bindParam(':id',$id, PDO::PARAM_INT);
	$stmt->execute();
	return $stmt->fetch(PDO::FETCH_OBJ);
	}
	
	// TRUNCATE TABLE
	public function TruncateTable($redirect_to)
	{
	$db=DB::getInstance();
   	$sql=$db->prepare("TRUNCATE setting_web");
	$sql->execute();
	send_to($redirect_to);
	}
	
	// DELETE
	public function Delete($id,$redirect_to)
	{
	$dbc = new dboptions();
	$dbc->dbDelete('setting_web', 'id',$id);
	send_to($redirect_to);
	}
	
	// INSERT
	public function Insert($name_web,$decription,$keyword,$open_web,$contact_us)
	{
	$dbc=new dboptions();

	$newupload = new UploadControl;
	$uploadname=$newupload->ImageUplaodResize('logo_web',THUMB_IMAGE_WIDTH,BIG_IMAGE_WIDTH,UPLOAD_PATH,THUMB_PATH,90);
	if($uploadname==''){
	$values = array(array( 'name_web'=>$name_web,'decription'=>$decription,'keyword'=>$keyword,'open_web'=>$open_web,'contact_us'=>$contact_us ));
	}else{
	$values = array(array( 'logo_web'=>$uploadname,'name_web'=>$name_web,'decription'=>$decription,'keyword'=>$keyword,'open_web'=>$open_web,'contact_us'=>$contact_us ));
	}
	$dbc->dbInsert('setting_web', $values);
	}
	
	// UPDATE
	public function Update($name_web,$decription,$keyword,$open_web,$contact_us,$id)
	{
	$db=DB::getInstance();
	$newupload = new UploadControl;
	$uploadname=$newupload->ImageUplaodResize('logo_web',THUMB_IMAGE_WIDTH,BIG_IMAGE_WIDTH,UPLOAD_PATH,THUMB_PATH,90);
	if($uploadname==''){
	$sql = " UPDATE setting_web SET  name_web =:name_web,decription =:decription,keyword =:keyword,open_web =:open_web,contact_us =:contact_us WHERE id = :id ";
	$data = array(':name_web'=>$name_web,':decription'=>$decription,':keyword'=>$keyword,':open_web'=>$open_web,':contact_us'=>$contact_us,':id'=>$id);
	}else{
	$sql = " UPDATE setting_web SET  logo_web=:logo_web,name_web =:name_web,decription =:decription,keyword =:keyword,open_web =:open_web,contact_us =:contact_us WHERE id = :id ";
	$data = array(':logo_web'=>$uploadname,':name_web'=>$name_web,':decription'=>$decription,':keyword'=>$keyword,':open_web'=>$open_web,':contact_us'=>$contact_us,':id'=>$id);
	}
	$stmt=$db->prepare($sql);
	$stmt->execute($data);
	
	}
	
	
	} // end class
	
	?>
	
	