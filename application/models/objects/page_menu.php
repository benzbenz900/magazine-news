
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        page_menu_model
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		page_menu
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include_once(APP_FOLDER.'/models/classes/class_page_menu.php');
	
	class page_menu_model{
	
	// SELECT ALL
	public function SelectAll($limit=null)
	{
	$dbc = new dboptions();
	if($limit){
	$startpg = pageparam($limit);
	$record = $dbc->rawSelect ("SELECT * FROM page_menu LIMIT {$startpg} , {$limit}");
	}else{
	$record = $dbc->rawSelect ("SELECT * FROM page_menu");	
	}
	return $record->fetchAll(PDO::FETCH_OBJ);
	}
	
	//Select Count for Pagination
	public function CountRow()
	{
	$dbc = new dboptions();
	return $dbc->SelectCount('SELECT COUNT(*) as num FROM page_menu');
	}
	
	// SELECT ONE
	public function SelectOne($id)
	{
	$db=DB::getInstance();
    $sql = "SELECT * FROM page_menu WHERE id=:id";
	$stmt=$db->prepare($sql);
	$stmt->bindParam(':id',$id, PDO::PARAM_INT);
	$stmt->execute();
	return $stmt->fetch(PDO::FETCH_OBJ);
	}
	
	// TRUNCATE TABLE
	public function TruncateTable($redirect_to)
	{
	$db=DB::getInstance();
   	$sql=$db->prepare("TRUNCATE page_menu");
	$sql->execute();
	send_to($redirect_to);
	}
	
	// DELETE
	public function Delete($id,$redirect_to)
	{
	$dbc = new dboptions();
	$dbc->dbDelete('page_menu', 'id',$id);
	send_to($redirect_to);
	}
	
	// INSERT
	public function Insert($name,$show_lp,$detail,$link,$keyword,$descirption,$order_lp)
	{
	$dbc=new dboptions();

	
	$values = array(array( 'name'=>$name,'show_lp'=>$show_lp,'detail'=>$detail,'link'=>$link,'keyword'=>$keyword,'descirption'=>$descirption,'order_lp'=>$order_lp ));
	$dbc->dbInsert('page_menu', $values);
	}
	
	// UPDATE
	public function Update($name,$show_lp,$detail,$link,$keyword,$descirption,$order_lp,$id)
	{
	$db=DB::getInstance();
	$sql = " UPDATE page_menu SET  name =:name,show_lp =:show_lp,detail =:detail,link =:link,keyword =:keyword,descirption =:descirption,order_lp =:order_lp WHERE id = :id ";
	$data = array(':name'=>$name,':show_lp'=>$show_lp,':detail'=>$detail,':link'=>$link,':keyword'=>$keyword,':descirption'=>$descirption,':order_lp'=>$order_lp,':id'=>$id);
	$stmt=$db->prepare($sql);
	$stmt->execute($data);
	
	}
	
	
	} // end class
	
	?>
	
	