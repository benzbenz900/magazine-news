
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        news_model
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		news
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include_once(APP_FOLDER.'/models/classes/class_news.php');
	
	class news_model{
	
	// SELECT ALL
	public function SelectAll($limit=null)
	{
	$dbc = new dboptions();
	if($limit){
	$startpg = pageparam($limit);
	$record = $dbc->rawSelect ("SELECT * FROM news LIMIT {$startpg} , {$limit}");
	}else{
	$record = $dbc->rawSelect ("SELECT * FROM news");	
	}
	return $record->fetchAll(PDO::FETCH_OBJ);
	}
	
	//Select Count for Pagination
	public function CountRow()
	{
	$dbc = new dboptions();
	return $dbc->SelectCount('SELECT COUNT(*) as num FROM news');
	}
	
	// SELECT ONE
	public function SelectOne($id)
	{
	$db=DB::getInstance();
    $sql = "SELECT * FROM news WHERE id=:id";
	$stmt=$db->prepare($sql);
	$stmt->bindParam(':id',$id, PDO::PARAM_INT);
	$stmt->execute();
	return $stmt->fetch(PDO::FETCH_OBJ);
	}
	
	// TRUNCATE TABLE
	public function TruncateTable($redirect_to)
	{
	$db=DB::getInstance();
   	$sql=$db->prepare("TRUNCATE news");
	$sql->execute();
	send_to($redirect_to);
	}
	
	// DELETE
	public function Delete($id,$redirect_to)
	{
	$dbc = new dboptions();
	$dbc->dbDelete('news', 'id',$id);
	send_to($redirect_to);
	}
	
	// INSERT
	public function Insert($name,$type,$type2,$type3,$type4,$type5,$detail,$date_add,$date_update,$view,$addby,$label)
	{
	$dbc=new dboptions();

	$newupload = new UploadControl;
	$uploadname=$newupload->ImageUplaodResize('image',THUMB_IMAGE_WIDTH,BIG_IMAGE_WIDTH,UPLOAD_PATH,THUMB_PATH,90);
	if($uploadname==''){
	$values = array(array( 'name'=>$name,'type'=>$type,'type2'=>$type2,'type3'=>$type3,'type4'=>$type4,'type5'=>$type5,'detail'=>$detail,'date_add'=>$date_add,'date_update'=>$date_update,'view'=>$view,'addby'=>$addby,'label'=>$label ));
	}else{
	$values = array(array( 'image'=>$uploadname,'name'=>$name,'type'=>$type,'type2'=>$type2,'type3'=>$type3,'type4'=>$type4,'type5'=>$type5,'detail'=>$detail,'date_add'=>$date_add,'date_update'=>$date_update,'view'=>$view,'addby'=>$addby,'label'=>$label ));
	}
	$dbc->dbInsert('news', $values);
	}
	
	// UPDATE
	public function Update($name,$type,$type2,$type3,$type4,$type5,$detail,$date_add,$date_update,$view,$addby,$label,$id)
	{
	$db=DB::getInstance();
	$newupload = new UploadControl;
	$uploadname=$newupload->ImageUplaodResize('image',THUMB_IMAGE_WIDTH,BIG_IMAGE_WIDTH,UPLOAD_PATH,THUMB_PATH,90);
	if($uploadname==''){
	$sql = " UPDATE news SET  name =:name,type =:type,type2 =:type2,type3 =:type3,type4 =:type4,type5 =:type5,detail =:detail,date_add =:date_add,date_update =:date_update,view =:view,addby =:addby,label =:label WHERE id = :id ";
	$data = array(':name'=>$name,':type'=>$type,':type2'=>$type2,':type3'=>$type3,':type4'=>$type4,':type5'=>$type5,':detail'=>$detail,':date_add'=>$date_add,':date_update'=>$date_update,':view'=>$view,':addby'=>$addby,':label'=>$label,':id'=>$id);
	}else{
	$sql = " UPDATE news SET  image=:image,name =:name,type =:type,type2 =:type2,type3 =:type3,type4 =:type4,type5 =:type5,detail =:detail,date_add =:date_add,date_update =:date_update,view =:view,addby =:addby,label =:label WHERE id = :id ";
	$data = array(':image'=>$uploadname,':name'=>$name,':type'=>$type,':type2'=>$type2,':type3'=>$type3,':type4'=>$type4,':type5'=>$type5,':detail'=>$detail,':date_add'=>$date_add,':date_update'=>$date_update,':view'=>$view,':addby'=>$addby,':label'=>$label,':id'=>$id);
	}
	$stmt=$db->prepare($sql);
	$stmt->execute($data);
	
	}
	
	
	} // end class
	
	?>
	
	