
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        category_model
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		category
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include_once(APP_FOLDER.'/models/classes/class_category.php');
	
	class category_model{
	
	// SELECT ALL
	public function SelectAll($limit=null)
	{
	$dbc = new dboptions();
	if($limit){
	$startpg = pageparam($limit);
	$record = $dbc->rawSelect ("SELECT * FROM category LIMIT {$startpg} , {$limit}");
	}else{
	$record = $dbc->rawSelect ("SELECT * FROM category");	
	}
	return $record->fetchAll(PDO::FETCH_OBJ);
	}
	
	//Select Count for Pagination
	public function CountRow()
	{
	$dbc = new dboptions();
	return $dbc->SelectCount('SELECT COUNT(*) as num FROM category');
	}
	
	// SELECT ONE
	public function SelectOne($id)
	{
	$db=DB::getInstance();
    $sql = "SELECT * FROM category WHERE id=:id";
	$stmt=$db->prepare($sql);
	$stmt->bindParam(':id',$id, PDO::PARAM_INT);
	$stmt->execute();
	return $stmt->fetch(PDO::FETCH_OBJ);
	}
	
	// TRUNCATE TABLE
	public function TruncateTable($redirect_to)
	{
	$db=DB::getInstance();
   	$sql=$db->prepare("TRUNCATE category");
	$sql->execute();
	send_to($redirect_to);
	}
	
	// DELETE
	public function Delete($id,$redirect_to)
	{
	$dbc = new dboptions();
	$dbc->dbDelete('category', 'id',$id);
	send_to($redirect_to);
	}
	
	// INSERT
	public function Insert($name,$detail,$descirption,$keyword)
	{
	$dbc=new dboptions();

	
	$values = array(array( 'name'=>$name,'detail'=>$detail,'descirption'=>$descirption,'keyword'=>$keyword ));
	$dbc->dbInsert('category', $values);
	}
	
	// UPDATE
	public function Update($name,$detail,$descirption,$keyword,$id)
	{
	$db=DB::getInstance();
	$sql = " UPDATE category SET  name =:name,detail =:detail,descirption =:descirption,keyword =:keyword WHERE id = :id ";
	$data = array(':name'=>$name,':detail'=>$detail,':descirption'=>$descirption,':keyword'=>$keyword,':id'=>$id);
	$stmt=$db->prepare($sql);
	$stmt->execute($data);
	
	}
	
	
	} // end class
	
	?>
	
	