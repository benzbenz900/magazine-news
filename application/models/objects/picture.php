
	<?php
	/*
	* =======================================================================
	* CLASSNAME:        picture_model
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		picture
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include_once(APP_FOLDER.'/models/classes/class_picture.php');
	
	class picture_model{
	
	// SELECT ALL
	public function SelectAll($limit=null)
	{
	$dbc = new dboptions();
	if($limit){
	$startpg = pageparam($limit);
	$record = $dbc->rawSelect ("SELECT * FROM picture LIMIT {$startpg} , {$limit}");
	}else{
	$record = $dbc->rawSelect ("SELECT * FROM picture");	
	}
	return $record->fetchAll(PDO::FETCH_OBJ);
	}
	
	//Select Count for Pagination
	public function CountRow()
	{
	$dbc = new dboptions();
	return $dbc->SelectCount('SELECT COUNT(*) as num FROM picture');
	}
	
	// SELECT ONE
	public function SelectOne($id)
	{
	$db=DB::getInstance();
    $sql = "SELECT * FROM picture WHERE id=:id";
	$stmt=$db->prepare($sql);
	$stmt->bindParam(':id',$id, PDO::PARAM_INT);
	$stmt->execute();
	return $stmt->fetch(PDO::FETCH_OBJ);
	}
	
	// TRUNCATE TABLE
	public function TruncateTable($redirect_to)
	{
	$db=DB::getInstance();
   	$sql=$db->prepare("TRUNCATE picture");
	$sql->execute();
	send_to($redirect_to);
	}
	
	// DELETE
	public function Delete($id,$redirect_to)
	{
	$dbc = new dboptions();
	$dbc->dbDelete('picture', 'id',$id);
	send_to($redirect_to);
	}
	
	// INSERT
	public function Insert($name)
	{
	$dbc=new dboptions();

	$newupload = new UploadControl;
	$uploadname=$newupload->ImageUplaodResize('image',THUMB_IMAGE_WIDTH,BIG_IMAGE_WIDTH,UPLOAD_PATH,THUMB_PATH,90);
	if($uploadname==''){
	$values = array(array( 'name'=>$name ));
	}else{
	$values = array(array( 'image'=>$uploadname,'name'=>$name ));
	}
	$dbc->dbInsert('picture', $values);
	}
	
	// UPDATE
	public function Update($name,$id)
	{
	$db=DB::getInstance();
	$newupload = new UploadControl;
	$uploadname=$newupload->ImageUplaodResize('image',THUMB_IMAGE_WIDTH,BIG_IMAGE_WIDTH,UPLOAD_PATH,THUMB_PATH,90);
	if($uploadname==''){
	$sql = " UPDATE picture SET  name =:name WHERE id = :id ";
	$data = array(':name'=>$name,':id'=>$id);
	}else{
	$sql = " UPDATE picture SET  image=:image,name =:name WHERE id = :id ";
	$data = array(':image'=>$uploadname,':name'=>$name,':id'=>$id);
	}
	$stmt=$db->prepare($sql);
	$stmt->execute($data);
	
	}
	
	
	} // end class
	
	?>
	
	