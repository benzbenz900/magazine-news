<?php
	/*
	* =======================================================================
	* FILE NAME:        .php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	//ad_manager	
	if(get('view')=='ad_manager'){
	include(APP_FOLDER.'/controllers/admin/ad_manager.php');
	$_controller = new ad_manager_controller();
	$_controller->invoke_ad_manager();
	}
	//category	
	if(get('view')=='category'){
	include(APP_FOLDER.'/controllers/admin/category.php');
	$_controller = new category_controller();
	$_controller->invoke_category();
	}
	//category_show	
	if(get('view')=='category_show'){
	include(APP_FOLDER.'/controllers/admin/category_show.php');
	$_controller = new category_show_controller();
	$_controller->invoke_category_show();
	}
	//news	
	if(get('view')=='news'){
	include(APP_FOLDER.'/controllers/admin/news.php');
	$_controller = new news_controller();
	$_controller->invoke_news();
	}
	//page_menu	
	if(get('view')=='page_menu'){
	include(APP_FOLDER.'/controllers/admin/page_menu.php');
	$_controller = new page_menu_controller();
	$_controller->invoke_page_menu();
	}
	//picture	
	if(get('view')=='picture'){
	include(APP_FOLDER.'/controllers/admin/picture.php');
	$_controller = new picture_controller();
	$_controller->invoke_picture();
	}
	//setting_web	
	if(get('view')=='setting_web'){
	include(APP_FOLDER.'/controllers/admin/setting_web.php');
	$_controller = new setting_web_controller();
	$_controller->invoke_setting_web();
	}
	//users	
	if(get('view')=='users'){
	include(APP_FOLDER.'/controllers/admin/users.php');
	$_controller = new users_controller();
	$_controller->invoke_users();
	}
	?>