
	<?php
	
	/*
	* =======================================================================
	* FILE NAME:        setting_web.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		setting_web
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include(APP_FOLDER.'/models/objects/setting_web.php');
	
	class setting_web_controller {
	public $setting_web_model;
	
	public function __construct()  
    {  
        $result = $this->setting_web_model = new setting_web_model();
    } 
	
	public function invoke_setting_web()
	{
	
	//SELECT ALL //////////////////////////////////	
	if(get('do')=='viewall'){
	if(PAGINATION_TYPE=='Normal'){
	$result = $this->setting_web_model->SelectAll(RECORD_PER_PAGE);
	//Accept get url  e.g (index.php?id=1&cat=2...)
	$paging = pagination($this->setting_web_model->CountRow(),RECORD_PER_PAGE,''.H_ADMIN.'&view=setting_web&do=viewall');
	}else{
	$result = $this->setting_web_model->SelectAll();	
	}
	include(APP_FOLDER.'/views/admin/setting_web/View.php');
	}
	
	
	//EXPORT ////////////////////////////////////////////////////	
	if(get('do')=='export'){
	$result = $this->setting_web_model->SelectAll();
	include(APP_FOLDER.'/views/admin/setting_web/Export.php');
	}
	
	//Expeort2
	elseif(get('do')=='export2'){
	$rows = $this->setting_web_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/setting_web/Export2.php');
	}
	
	//ADD //////////////////////////////////////////////////
	elseif(get('do')=='add'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/		 
	$this->setting_web_model->Insert(post('name_web'),post('decription'),post('keyword'),post('open_web'),post('contact_us'),post('id'));
	send_to(''.H_ADMIN.'&view=setting_web&do=viewall&msg=add');
	}
	//} //uncomment for validation
	include(APP_FOLDER.'/views/admin/setting_web/Add.php');
	}
	
	//UPDATE ////////////////////////////////////////////////
	elseif(get('do')=='update'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/	 
	$this->setting_web_model->Update(post('name_web'),post('decription'),post('keyword'),post('open_web'),post('contact_us'),post('id'));
	send_to(''.H_ADMIN.'&view=setting_web&id='.get('id').'&do=details&msg=update');
	//} //uncomment for validation
	}
	$rows = $this->setting_web_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/setting_web/Update.php');
	}
	
	//DETAILS //////////////////////////////////////////////
	elseif(get('do')=='details'){
	$rows = $this->setting_web_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/setting_web/Details.php');
	}
	
	//DELETE /////////////////////////////////////////////////
	elseif(get('do')=='delete'){
	$dfile=get('dfile');
		if(get('id') and $dfile==''){
	$del = $this->setting_web_model->Delete(get('id'),''.H_ADMIN.'&view=setting_web&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')==''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	$del = $this->setting_web_model->Delete(get('id'),''.H_ADMIN.'&view=setting_web&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')!=''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	send_to(''.H_ADMIN.'&view=setting_web&id='.get('id').'&do=update&msg=delete');
	}
	}
	}//end invoke
	}//end class
	?>
	