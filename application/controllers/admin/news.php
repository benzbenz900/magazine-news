
	<?php
	
	/*
	* =======================================================================
	* FILE NAME:        news.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		news
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include(APP_FOLDER.'/models/objects/news.php');
	
	class news_controller {
	public $news_model;
	
	public function __construct()  
    {  
        $result = $this->news_model = new news_model();
    } 
	
	public function invoke_news()
	{
	
	//SELECT ALL //////////////////////////////////	
	if(get('do')=='viewall'){
	if(PAGINATION_TYPE=='Normal'){
	$result = $this->news_model->SelectAll(RECORD_PER_PAGE);
	//Accept get url  e.g (index.php?id=1&cat=2...)
	$paging = pagination($this->news_model->CountRow(),RECORD_PER_PAGE,''.H_ADMIN.'&view=news&do=viewall');
	}else{
	$result = $this->news_model->SelectAll();	
	}
	include(APP_FOLDER.'/views/admin/news/View.php');
	}
	
	
	//EXPORT ////////////////////////////////////////////////////	
	if(get('do')=='export'){
	$result = $this->news_model->SelectAll();
	include(APP_FOLDER.'/views/admin/news/Export.php');
	}
	
	//Expeort2
	elseif(get('do')=='export2'){
	$rows = $this->news_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/news/Export2.php');
	}
	
	//ADD //////////////////////////////////////////////////
	elseif(get('do')=='add'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/		 
	$this->news_model->Insert(post('name'),post('type'),post('type2'),post('type3'),post('type4'),post('type5'),post('detail'),post('date_add'),post('date_update'),post('view'),post('addby'),post('label'),post('id'));
	send_to(''.H_ADMIN.'&view=news&do=viewall&msg=add');
	}
	//} //uncomment for validation
	include(APP_FOLDER.'/views/admin/news/Add.php');
	}
	
	//UPDATE ////////////////////////////////////////////////
	elseif(get('do')=='update'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/	 
	$this->news_model->Update(post('name'),post('type'),post('type2'),post('type3'),post('type4'),post('type5'),post('detail'),post('date_add'),post('date_update'),post('view'),post('addby'),post('label'),post('id'));
	send_to(''.H_ADMIN.'&view=news&id='.get('id').'&do=details&msg=update');
	//} //uncomment for validation
	}
	$rows = $this->news_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/news/Update.php');
	}
	
	//DETAILS //////////////////////////////////////////////
	elseif(get('do')=='details'){
	$rows = $this->news_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/news/Details.php');
	}
	
	//DELETE /////////////////////////////////////////////////
	elseif(get('do')=='delete'){
	$dfile=get('dfile');
		if(get('id') and $dfile==''){
	$del = $this->news_model->Delete(get('id'),''.H_ADMIN.'&view=news&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')==''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	$del = $this->news_model->Delete(get('id'),''.H_ADMIN.'&view=news&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')!=''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	send_to(''.H_ADMIN.'&view=news&id='.get('id').'&do=update&msg=delete');
	}
	}
	}//end invoke
	}//end class
	?>
	