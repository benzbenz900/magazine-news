
	<?php
	
	/*
	* =======================================================================
	* FILE NAME:        category.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		category
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include(APP_FOLDER.'/models/objects/category.php');
	
	class category_controller {
	public $category_model;
	
	public function __construct()  
    {  
        $result = $this->category_model = new category_model();
    } 
	
	public function invoke_category()
	{
	
	//SELECT ALL //////////////////////////////////	
	if(get('do')=='viewall'){
	if(PAGINATION_TYPE=='Normal'){
	$result = $this->category_model->SelectAll(RECORD_PER_PAGE);
	//Accept get url  e.g (index.php?id=1&cat=2...)
	$paging = pagination($this->category_model->CountRow(),RECORD_PER_PAGE,''.H_ADMIN.'&view=category&do=viewall');
	}else{
	$result = $this->category_model->SelectAll();	
	}
	include(APP_FOLDER.'/views/admin/category/View.php');
	}
	
	
	//EXPORT ////////////////////////////////////////////////////	
	if(get('do')=='export'){
	$result = $this->category_model->SelectAll();
	include(APP_FOLDER.'/views/admin/category/Export.php');
	}
	
	//Expeort2
	elseif(get('do')=='export2'){
	$rows = $this->category_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/category/Export2.php');
	}
	
	//ADD //////////////////////////////////////////////////
	elseif(get('do')=='add'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/		 
	$this->category_model->Insert(post('name'),post('detail'),post('descirption'),post('keyword'),post('id'));
	send_to(''.H_ADMIN.'&view=category&do=viewall&msg=add');
	}
	//} //uncomment for validation
	include(APP_FOLDER.'/views/admin/category/Add.php');
	}
	
	//UPDATE ////////////////////////////////////////////////
	elseif(get('do')=='update'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/	 
	$this->category_model->Update(post('name'),post('detail'),post('descirption'),post('keyword'),post('id'));
	send_to(''.H_ADMIN.'&view=category&id='.get('id').'&do=details&msg=update');
	//} //uncomment for validation
	}
	$rows = $this->category_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/category/Update.php');
	}
	
	//DETAILS //////////////////////////////////////////////
	elseif(get('do')=='details'){
	$rows = $this->category_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/category/Details.php');
	}
	
	//DELETE /////////////////////////////////////////////////
	elseif(get('do')=='delete'){
	$dfile=get('dfile');
		if(get('id') and $dfile==''){
	$del = $this->category_model->Delete(get('id'),''.H_ADMIN.'&view=category&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')==''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	$del = $this->category_model->Delete(get('id'),''.H_ADMIN.'&view=category&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')!=''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	send_to(''.H_ADMIN.'&view=category&id='.get('id').'&do=update&msg=delete');
	}
	}
	}//end invoke
	}//end class
	?>
	