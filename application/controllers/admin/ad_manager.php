
	<?php
	
	/*
	* =======================================================================
	* FILE NAME:        ad_manager.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		ad_manager
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include(APP_FOLDER.'/models/objects/ad_manager.php');
	
	class ad_manager_controller {
	public $ad_manager_model;
	
	public function __construct()  
    {  
        $result = $this->ad_manager_model = new ad_manager_model();
    } 
	
	public function invoke_ad_manager()
	{
	
	//SELECT ALL //////////////////////////////////	
	if(get('do')=='viewall'){
	if(PAGINATION_TYPE=='Normal'){
	$result = $this->ad_manager_model->SelectAll(RECORD_PER_PAGE);
	//Accept get url  e.g (index.php?id=1&cat=2...)
	$paging = pagination($this->ad_manager_model->CountRow(),RECORD_PER_PAGE,''.H_ADMIN.'&view=ad_manager&do=viewall');
	}else{
	$result = $this->ad_manager_model->SelectAll();	
	}
	include(APP_FOLDER.'/views/admin/ad_manager/View.php');
	}
	
	
	//EXPORT ////////////////////////////////////////////////////	
	if(get('do')=='export'){
	$result = $this->ad_manager_model->SelectAll();
	include(APP_FOLDER.'/views/admin/ad_manager/Export.php');
	}
	
	//Expeort2
	elseif(get('do')=='export2'){
	$rows = $this->ad_manager_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/ad_manager/Export2.php');
	}
	
	//ADD //////////////////////////////////////////////////
	elseif(get('do')=='add'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/		 
	$this->ad_manager_model->Insert(post('name_ad'),post('codelink'),post('show_lp'),post('end_date'),post('position'),post('id'));
	send_to(''.H_ADMIN.'&view=ad_manager&do=viewall&msg=add');
	}
	//} //uncomment for validation
	include(APP_FOLDER.'/views/admin/ad_manager/Add.php');
	}
	
	//UPDATE ////////////////////////////////////////////////
	elseif(get('do')=='update'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/	 
	$this->ad_manager_model->Update(post('name_ad'),post('codelink'),post('show_lp'),post('end_date'),post('position'),post('id'));
	send_to(''.H_ADMIN.'&view=ad_manager&id='.get('id').'&do=details&msg=update');
	//} //uncomment for validation
	}
	$rows = $this->ad_manager_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/ad_manager/Update.php');
	}
	
	//DETAILS //////////////////////////////////////////////
	elseif(get('do')=='details'){
	$rows = $this->ad_manager_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/ad_manager/Details.php');
	}
	
	//DELETE /////////////////////////////////////////////////
	elseif(get('do')=='delete'){
	$dfile=get('dfile');
		if(get('id') and $dfile==''){
	$del = $this->ad_manager_model->Delete(get('id'),''.H_ADMIN.'&view=ad_manager&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')==''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	$del = $this->ad_manager_model->Delete(get('id'),''.H_ADMIN.'&view=ad_manager&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')!=''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	send_to(''.H_ADMIN.'&view=ad_manager&id='.get('id').'&do=update&msg=delete');
	}
	}
	}//end invoke
	}//end class
	?>
	