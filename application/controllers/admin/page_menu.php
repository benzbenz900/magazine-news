
	<?php
	
	/*
	* =======================================================================
	* FILE NAME:        page_menu.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		page_menu
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include(APP_FOLDER.'/models/objects/page_menu.php');
	
	class page_menu_controller {
	public $page_menu_model;
	
	public function __construct()  
    {  
        $result = $this->page_menu_model = new page_menu_model();
    } 
	
	public function invoke_page_menu()
	{
	
	//SELECT ALL //////////////////////////////////	
	if(get('do')=='viewall'){
	if(PAGINATION_TYPE=='Normal'){
	$result = $this->page_menu_model->SelectAll(RECORD_PER_PAGE);
	//Accept get url  e.g (index.php?id=1&cat=2...)
	$paging = pagination($this->page_menu_model->CountRow(),RECORD_PER_PAGE,''.H_ADMIN.'&view=page_menu&do=viewall');
	}else{
	$result = $this->page_menu_model->SelectAll();	
	}
	include(APP_FOLDER.'/views/admin/page_menu/View.php');
	}
	
	
	//EXPORT ////////////////////////////////////////////////////	
	if(get('do')=='export'){
	$result = $this->page_menu_model->SelectAll();
	include(APP_FOLDER.'/views/admin/page_menu/Export.php');
	}
	
	//Expeort2
	elseif(get('do')=='export2'){
	$rows = $this->page_menu_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/page_menu/Export2.php');
	}
	
	//ADD //////////////////////////////////////////////////
	elseif(get('do')=='add'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/		 
	$this->page_menu_model->Insert(post('name'),post('show_lp'),post('detail'),post('link'),post('keyword'),post('descirption'),post('order_lp'),post('id'));
	send_to(''.H_ADMIN.'&view=page_menu&do=viewall&msg=add');
	}
	//} //uncomment for validation
	include(APP_FOLDER.'/views/admin/page_menu/Add.php');
	}
	
	//UPDATE ////////////////////////////////////////////////
	elseif(get('do')=='update'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/	 
	$this->page_menu_model->Update(post('name'),post('show_lp'),post('detail'),post('link'),post('keyword'),post('descirption'),post('order_lp'),post('id'));
	send_to(''.H_ADMIN.'&view=page_menu&id='.get('id').'&do=details&msg=update');
	//} //uncomment for validation
	}
	$rows = $this->page_menu_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/page_menu/Update.php');
	}
	
	//DETAILS //////////////////////////////////////////////
	elseif(get('do')=='details'){
	$rows = $this->page_menu_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/page_menu/Details.php');
	}
	
	//DELETE /////////////////////////////////////////////////
	elseif(get('do')=='delete'){
	$dfile=get('dfile');
		if(get('id') and $dfile==''){
	$del = $this->page_menu_model->Delete(get('id'),''.H_ADMIN.'&view=page_menu&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')==''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	$del = $this->page_menu_model->Delete(get('id'),''.H_ADMIN.'&view=page_menu&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')!=''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	send_to(''.H_ADMIN.'&view=page_menu&id='.get('id').'&do=update&msg=delete');
	}
	}
	}//end invoke
	}//end class
	?>
	