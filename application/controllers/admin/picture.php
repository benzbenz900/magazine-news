
	<?php
	
	/*
	* =======================================================================
	* FILE NAME:        picture.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		picture
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	
	include(APP_FOLDER.'/models/objects/picture.php');
	
	class picture_controller {
	public $picture_model;
	
	public function __construct()  
    {  
        $result = $this->picture_model = new picture_model();
    } 
	
	public function invoke_picture()
	{
	
	//SELECT ALL //////////////////////////////////	
	if(get('do')=='viewall'){
	if(PAGINATION_TYPE=='Normal'){
	$result = $this->picture_model->SelectAll(RECORD_PER_PAGE);
	//Accept get url  e.g (index.php?id=1&cat=2...)
	$paging = pagination($this->picture_model->CountRow(),RECORD_PER_PAGE,''.H_ADMIN.'&view=picture&do=viewall');
	}else{
	$result = $this->picture_model->SelectAll();	
	}
	include(APP_FOLDER.'/views/admin/picture/View.php');
	}
	
	
	//EXPORT ////////////////////////////////////////////////////	
	if(get('do')=='export'){
	$result = $this->picture_model->SelectAll();
	include(APP_FOLDER.'/views/admin/picture/Export.php');
	}
	
	//Expeort2
	elseif(get('do')=='export2'){
	$rows = $this->picture_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/picture/Export2.php');
	}
	
	//ADD //////////////////////////////////////////////////
	elseif(get('do')=='add'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/		 
	$this->picture_model->Insert(post('name'),post('id'));
	send_to(''.H_ADMIN.'&view=picture&do=viewall&msg=add');
	}
	//} //uncomment for validation
	include(APP_FOLDER.'/views/admin/picture/Add.php');
	}
	
	//UPDATE ////////////////////////////////////////////////
	elseif(get('do')=='update'){
	if(post('button')){
	//validation example - uncomment to make use of it.
	/*if (post('fieldname')==''){
		$errors[] = 'A value is required!';
	}else{
	*/	 
	$this->picture_model->Update(post('name'),post('id'));
	send_to(''.H_ADMIN.'&view=picture&id='.get('id').'&do=details&msg=update');
	//} //uncomment for validation
	}
	$rows = $this->picture_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/picture/Update.php');
	}
	
	//DETAILS //////////////////////////////////////////////
	elseif(get('do')=='details'){
	$rows = $this->picture_model->SelectOne(get('id'));
	include(APP_FOLDER.'/views/admin/picture/Details.php');
	}
	
	//DELETE /////////////////////////////////////////////////
	elseif(get('do')=='delete'){
	$dfile=get('dfile');
		if(get('id') and $dfile==''){
	$del = $this->picture_model->Delete(get('id'),''.H_ADMIN.'&view=picture&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')==''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	$del = $this->picture_model->Delete(get('id'),''.H_ADMIN.'&view=picture&do=viewall&msg=delete');
	}
	elseif(get('id') and $dfile!='' and get('fdel')!=''){
	delete_files(UPLOAD_PATH.get('dfile'));
	delete_files(THUMB_PATH.get('dfile'));
	send_to(''.H_ADMIN.'&view=picture&id='.get('id').'&do=update&msg=delete');
	}
	}
	}//end invoke
	}//end class
	?>
	