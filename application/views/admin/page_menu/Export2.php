
	<?php
	/*
	* =======================================================================
	* FILE NAME:        page_menu.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		page_menu
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	<?php
	$etype=get('etype');
	$excel='
	<p style="font-family:arial; font-size:18px;" align="left">
	<strong style="font-family:arial;">'.LANG_REPORT_TITLE.'</strong><br>'.LANG_REPORT_SUB_TITLE.'<br>
	<strong>'.LANG_REPORT_TABLE.'</strong> Page Menu</p>';
	$excel.='
	<table border="1" cellspacing="0" width="100%" style="font-family:arial; font-size:14px;" cellpadding="5">
    <tr>
	<td>Name</td>
	<td>'.$rows->name.'</td>
  	</tr>
    <tr>
	<td>Show Lp</td>
	<td>'.$rows->show_lp.'</td>
  	</tr>
    <tr>
	<td>Detail</td>
	<td>'.$rows->detail.'</td>
  	</tr>
    <tr>
	<td>Link</td>
	<td>'.$rows->link.'</td>
  	</tr>
    <tr>
	<td>Keyword</td>
	<td>'.$rows->keyword.'</td>
  	</tr>
    <tr>
	<td>Descirption</td>
	<td>'.$rows->descirption.'</td>
  	</tr>
    <tr>
	<td>Order Lp</td>
	<td>'.$rows->order_lp.'</td>
  	</tr>';
   $excel.='</table>';
	
	$filename1= 'page_menu_'.date('Y-m-d').'.doc';
	$filename2= 'page_menu_'.date('Y-m-d').'.xls';
	$pdf_output= 'page_menu_'.date('Y-m-d').'.pdf';
	if ($etype == 'word') {
	header("Content-type: application/msword");
	header("Content-Disposition: attachment; filename=$filename1");
	header("Pragma: no-cache");
	header("Expires: 0");
	print $excel;
	}
	elseif ($etype == 'excel') {
	header("Content-type: application/msexcel");
	header("Content-Disposition: attachment; filename=$filename2");
	header("Pragma: no-cache");
	header("Expires: 0");
	print $excel;
	}
	elseif ($etype == 'printer') {
	print'<title>'.H_TITLE.'</title>
	<script type="text/javascript">
	window.onload = function () {
		window.print();
	}
	</script>
	';
	print $excel;
	}
	elseif ($etype == 'PDF') {
	HezecomPDF($excel, $pdf_output);
	}
	?>