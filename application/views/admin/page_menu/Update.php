
	<?php
	/*
	* =======================================================================
	* FILE NAME:        page_menu.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		page_menu
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-12">
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=page_menu&id=<?php echo $rows->id;?>&do=details" title="View Details" class="btn btn-default btn-sm tip"><i class="fa fa-th-list"></i> <?php echo LANG_DETAILS;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=page_menu&id=<?php echo $rows->id;?>&do=delete&dfile=" title="<?php echo LANG_TIP_DELETE;?>" class="btn btn-default btn-sm tip" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><i class="fa fa-trash-o"></i> <?php echo LANG_DELETE;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=page_menu&do=viewall" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_VIEWALL;?>"><i class="fa fa-reply"></i> <?php echo LANG_GO_BACK;?></a>
	</ul>
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> <?php echo LANG_UPDATE;?> Page Menu</h3></div>
  <div class="panel-body pformmargin">
	
	 
	 <p>
	 <form action="<?php htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" name="hezecomform" id="hezecomform" enctype="multipart/form-data">
	
	<?php if(isset($errors))form_errors($errors);?>
	
	<input type="hidden" name="id" value="<?php echo $rows->id;?>">
	<div class="form-group">
    <label class="control-label" for="name">Name</label>
	<input id="name" name="name" type="text" maxlength="255"  value="<?php echo $rows->name;?>" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="show_lp">Show Lp</label>
	<select name="show_lp" id="show_lp" class="required form-control  choz">
	<option value="<?php echo $rows->show_lp;?>"><?php echo $rows->show_lp;?></option><option value="Show">Show</option>
	<option value="Not Show">Not Show</option>
	</select>
	</div>

	<div class="form-group">
    <label class="control-label" for="detail">Detail</label>
	
		<textarea rows="5" id="detail" name="detail" class="form-control " /><?php echo $rows->detail;?></textarea>
	<?php HezecomEditor('detail'); ?></div>

	<div class="form-group">
    <label class="control-label" for="link">Link</label>
	
		<textarea rows="5" id="link" name="link" class="form-control " /><?php echo $rows->link;?></textarea>
	<?php HezecomEditor('link'); ?></div>

	<div class="form-group">
    <label class="control-label" for="keyword">Keyword</label>
	<input id="keyword" name="keyword" type="text" maxlength="255"  value="<?php echo $rows->keyword;?>" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="descirption">Descirption</label>
	<input id="descirption" name="descirption" type="text" maxlength="255"  value="<?php echo $rows->descirption;?>" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="order_lp">Order Lp</label>
	<input id="order_lp" name="order_lp" type="text" maxlength="11"  value="<?php echo $rows->order_lp;?>" class="form-control " required/>
	</div>

	 <div class="controls">
	 <div class="col-md-2" style="padding:0;">
	 <input type="submit" name="button" id="hButton" class="btn btn-primary btn-lg" value="<?php echo LANG_UPDATE_RECORD;?>" />
	 </div>
	 <div class="col-md-3" style="padding:0;">
	 <div id="output"></div>
	 </div>
	 </div>
	  
	</form>
	</p>
	 
	
	</div>
 </div><!--/col-12-->
 </div><!--/heze-table-->
	