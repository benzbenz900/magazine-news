
	<?php
	/*
	* =======================================================================
	* FILE NAME:        news.php
	* DATE CREATED:  	01-06-2015
	* FOR TABLE:  		news
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-12">
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=news&id=<?php echo $rows->id;?>&do=details" title="View Details" class="btn btn-default btn-sm tip"><i class="fa fa-th-list"></i> <?php echo LANG_DETAILS;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=news&id=<?php echo $rows->id;?>&do=delete&dfile=<?php echo $rows->image;?>" title="<?php echo LANG_TIP_DELETE;?>" class="btn btn-default btn-sm tip" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><i class="fa fa-trash-o"></i> <?php echo LANG_DELETE;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=news&do=viewall" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_VIEWALL;?>"><i class="fa fa-reply"></i> <?php echo LANG_GO_BACK;?></a>
	</ul>
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> <?php echo LANG_UPDATE;?> News</h3></div>
  <div class="panel-body pformmargin">
	
	 
	 <p>
	 <form action="<?php htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" name="hezecomform" id="hezecomform" enctype="multipart/form-data">
	
	<?php if(isset($errors))form_errors($errors);?>
	
	<input type="hidden" name="id" value="<?php echo $rows->id;?>">
	<div class="form-group">
    <label class="control-label" for="image">Image</label>
	<input id="image" name="image"type="file" class="styler"/><br><?php if(is_file(UPLOAD_FOLDER.$rows->image)){?><a href="#"><img src="<?php echo THUMB_FOLDER.$rows->image;?>"></a><br><?php }?>
	<?php if(is_file(UPLOAD_FOLDER.$rows->image)){?>
	<a href="<?php echo H_ADMIN;?>&view=news&id=<?php echo $rows->id;?>&dfile=<?php echo $rows->image;?>&do=delete&fdel=file" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><span class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> <?php echo LANG_DELETE;?></span></a><br><?php }?>
	
	</div>

	<div class="form-group">
    <label class="control-label" for="name">Name</label>
	<input id="name" name="name" type="text" maxlength="255"  value="<?php echo $rows->name;?>" class="form-control " required/>
	</div>

	<?php
require_once 'libs/function/class.DBPDO.php';
$category = new DBPDO();
$categorys = $category->fetchAll("SELECT * FROM category");
?>
	<div class="form-group">
    <label class="control-label" for="type">Type</label>
    </div>
    <div class="form-group">
    <div class="row">
    <div class="col-md-2">
	<select name="type" id="type" class="required form-control  choz">
	<option value=""></option>
	<?php
		foreach ($categorys as $c) {
			if ($rows->type == $c['id']) {
				$ic = 'selected=""';
			}else{
				$ic = '';
			}
			echo '<option '.$ic.' value="'.$c['id'].'">'.$c['name'].'</option>';
		}
	?>
	</select>
	</div>
<div class="col-md-2">
<select name="type2" id="type2" class="required form-control  choz">
<option value=""></option>
	<?php
		foreach ($categorys as $c) {
			if ($rows->type2 == $c['id']) {
				$ic = 'selected=""';
			}else{
				$ic = '';
			}
			echo '<option '.$ic.' value="'.$c['id'].'">'.$c['name'].'</option>';
		}
	?>
	</select>
</div>
<div class="col-md-2">
	<select name="type3" id="type3" class="required form-control  choz">
	<option value=""></option>
	<?php
		foreach ($categorys as $c) {
			if ($rows->type3 == $c['id']) {
				$ic = 'selected=""';
			}else{
				$ic = '';
			}
			echo '<option '.$ic.' value="'.$c['id'].'">'.$c['name'].'</option>';
		}
	?>
	</select>
</div>
<div class="col-md-2">
	<select name="type4" id="type4" class="required form-control  choz">
	<option value=""></option>
	<?php
		foreach ($categorys as $c) {
			if ($rows->type4 == $c['id']) {
				$ic = 'selected=""';
			}else{
				$ic = '';
			}
			echo '<option '.$ic.' value="'.$c['id'].'">'.$c['name'].'</option>';
		}
	?>
	</select>
</div>
<div class="col-md-2">
	<select name="type5" id="type5" class="required form-control  choz">
	<option value=""></option>
	<?php
		foreach ($categorys as $c) {
			if ($rows->type5 == $c['id']) {
				$ic = 'selected=""';
			}else{
				$ic = '';
			}
			echo '<option '.$ic.' value="'.$c['id'].'">'.$c['name'].'</option>';
		}
	?>
	</select>
</div>
</div>
	</div>

	<div class="form-group">
    <label class="control-label" for="detail">Detail</label>
	
		<textarea rows="5" id="detail" name="detail" class="form-control " /><?php echo $rows->detail;?></textarea>
	<?php HezecomEditor('detail'); ?></div>

	<input name="date_add" class="datepicker form-control " type="hidden" maxlength="3" value="<?php echo $rows->date_add;?>"/>

	<input name="date_update" class="datepicker form-control " type="hidden" maxlength="3" value="<?php echo date('Y-m-d h:i:m');?>"/>

	<input id="view" name="view" type="hidden" maxlength="11"  value="<?php echo $rows->view;?>" class="form-control " />

	<input id="addby" name="addby" type="hidden" maxlength="255"  value="<?php echo $rows->addby;?>" class="form-control " />

<div class="form-group">
    <label class="control-label" for="label">Label ใช่ , คั้นระหว่างคำ</label>
	<input id="label" name="label" type="text" maxlength="255"  value="<?php echo $rows->label;?>" class="form-control "/>
	</div>

	 <div class="controls">
	 <div class="col-md-2" style="padding:0;">
	 <input type="submit" name="button" id="hButton" class="btn btn-primary btn-lg" value="<?php echo LANG_UPDATE_RECORD;?>" />
	 </div>
	 <div class="col-md-3" style="padding:0;">
	 <div id="output"></div>
	 </div>
	 </div>
	  
	</form>
	</p>
	 
	
	</div>
 </div><!--/col-12-->
 </div><!--/heze-table-->
	