
	<?php
	/*
	* =======================================================================
	* FILE NAME:        news.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		news
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	<?php
	$etype=get('etype');
	$excel='
	<p style="font-family:arial; font-size:18px;" align="left">
	<strong style="font-family:arial;">'.LANG_REPORT_TITLE.'</strong><br>'.LANG_REPORT_SUB_TITLE.'<br>
	<strong>'.LANG_REPORT_TABLE.'</strong> News</p>';
	$excel.='<table border="1" cellspacing="0" width="100%" style="font-family:arial; font-size:14px;" cellpadding="5">
	<tr>
      <th>Image</th>
      <th>Name</th>
      <th>Type</th>
      <th>Type2</th>
      <th>Type3</th>
      <th>Type4</th>
      <th>Type5</th>
      <th>Detail</th>
      <th>Date Add</th>
      <th>Date Update</th>
      <th>View</th>
      <th>Addby</th>
  </tr>
  ';
	foreach($result as $rows)
			{
	$excel.='<tr>
	<td>'.$rows->image.'</td>
	<td>'.$rows->name.'</td>
	<td>'.$rows->type.'</td>
	<td>'.$rows->type2.'</td>
	<td>'.$rows->type3.'</td>
	<td>'.$rows->type4.'</td>
	<td>'.$rows->type5.'</td>
	<td>'.$rows->detail.'</td>
	<td>'.$rows->date_add.'</td>
	<td>'.$rows->date_update.'</td>
	<td>'.$rows->view.'</td>
	<td>'.$rows->addby.'</td>
	</tr>';
	}
	$excel.='</table>';
	$filename1= 'news_'.date('Y-m-d').'.doc';
	$filename2= 'news_'.date('Y-m-d').'.xls';
	if ($etype == 'word') {
	header("Content-type: application/msword");
	header("Content-Disposition: attachment; filename=$filename1");
	header("Pragma: no-cache");
	header("Expires: 0");
	print $excel;
	}
	elseif ($etype == 'excel') {
	header("Content-type: application/msexcel");
	header("Content-Disposition: attachment; filename=$filename2");
	header("Pragma: no-cache");
	header("Expires: 0");
	print $excel;
	}
	elseif ($etype == 'printer') {
	print'<title>'.H_TITLE.'</title>
	<script type="text/javascript">
	window.onload = function () {
		window.print();
	}
	</script>
	';
	print $excel;
	}
	
	?>