
	<?php
	/*
	* =======================================================================
	* FILE NAME:        news.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		news
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-lg-12">
	
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=news&do=viewall" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_VIEWALL;?>"><i class="fa fa-reply"></i> <?php echo LANG_GO_BACK;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=news&do=add" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_ADD;?>"><i class="fa fa-plus"></i> <?php echo LANG_ADD;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=news&id=<?php echo $rows->id;?>&do=update" title="<?php echo LANG_TIP_UPDATE;?> Record" class="btn btn-default btn-sm tip"><i class="fa fa-edit"></i> <?php echo LANG_UPDATE;?></a>
	
	<a href="<?php echo H_ADMIN_MAIN;?>&view=news&id=<?php echo $rows->id;?>&do=export2&hexport=yes&etype=word" title="<?php echo LANG_TIP_WORD;?>" class="btn btn-default btn-sm tip"><i class="fa fa-file-o"></i> <?php echo LANG_WORD;?></a>
	
	<a href="<?php echo H_ADMIN_MAIN;?>&view=news&id=<?php echo $rows->id;?>&do=export2&hexport=yes&etype=printer" title="<?php echo LANG_TIP_PRINT;?>" target="_blank" class="btn btn-default btn-sm tip"><i class="fa fa-print"></i> <?php echo LANG_PRINT;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=news&id=<?php echo $rows->id;?>&do=delete&dfile=" title="<?php echo LANG_TIP_DELETE_ALL;?>" class="btn btn-default btn-sm tip" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><i class="fa fa-trash-o"></i> <?php echo LANG_DELETE;?></a>
	</ul>
	
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> News <?php echo LANG_DETAILS;?></h3></div>
	<table class="table table-striped table-bordered" data-page-size="200">
	 <tbody>
		  	
	<tr>
	<th>Image</th><td class='gallery'><?php if(is_file(UPLOAD_FOLDER.$rows->image)){?><a href='<?php echo UPLOAD_FOLDER.$rows->image;?>' data-rel='hezebox'><img src='<?php echo THUMB_FOLDER.$rows->image;?>'></a><?php }?></td>
	</tr>
		
	<tr>
	<th>Name</th><td><?php echo $rows->name;?></td>
	</tr>
		
	<tr>
	<th>Type</th><td><?php echo $rows->type;?></td>
	</tr>
		
	<tr>
	<th>Type2</th><td><?php echo $rows->type2;?></td>
	</tr>
		
	<tr>
	<th>Type3</th><td><?php echo $rows->type3;?></td>
	</tr>
		
	<tr>
	<th>Type4</th><td><?php echo $rows->type4;?></td>
	</tr>
		
	<tr>
	<th>Type5</th><td><?php echo $rows->type5;?></td>
	</tr>
		
	<tr>
	<th>Detail</th><td><?php echo $rows->detail;?></td>
	</tr>
		
	<tr>
	<th>Date Add</th><td><?php echo $rows->date_add;?></td>
	</tr>
		
	<tr>
	<th>Date Update</th><td><?php echo $rows->date_update;?></td>
	</tr>
		
	<tr>
	<th>View</th><td><?php echo $rows->view;?></td>
	</tr>
		
	<tr>
	<th>Addby</th><td><?php echo $rows->addby;?></td>
	</tr>
	</tbody>
	</table>
	</div>
 </div><!--/col-12-->
 </div><!--/heze-table-->
	