
	<?php
	/*
	* =======================================================================
	* FILE NAME:        category_show.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		category_show
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-12">
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=category_show&id=<?php echo $rows->id;?>&do=details" title="View Details" class="btn btn-default btn-sm tip"><i class="fa fa-th-list"></i> <?php echo LANG_DETAILS;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=category_show&id=<?php echo $rows->id;?>&do=delete&dfile=" title="<?php echo LANG_TIP_DELETE;?>" class="btn btn-default btn-sm tip" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><i class="fa fa-trash-o"></i> <?php echo LANG_DELETE;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=category_show&do=viewall" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_VIEWALL;?>"><i class="fa fa-reply"></i> <?php echo LANG_GO_BACK;?></a>
	</ul>
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> <?php echo LANG_UPDATE;?> Category Show</h3></div>
  <div class="panel-body pformmargin">
	
	 
	 <p>
	 <form action="<?php htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" name="hezecomform" id="hezecomform" enctype="multipart/form-data">
	
	<?php if(isset($errors))form_errors($errors);?>
	
	<input type="hidden" name="id" value="<?php echo $rows->id;?>">

		<?php
require_once 'libs/function/class.DBPDO.php';
$category = new DBPDO();
$categorys = $category->fetchAll("SELECT * FROM category");
?>
	<div class="form-group">
    <label class="control-label" for="type">Category</label>
	<select name="category" id="category" class="required form-control  choz">
	<?php
		foreach ($categorys as $c) {
			$catse = ($rows->category == $c['id']) ? 'selected' : '';
			echo '<option '.$catse.' value="'.$c['id'].'">'.$c['name'].'</option>';
		}
	?>
	</select>
	</div>

	<div class="form-group">
    <label class="control-label" for="categoryname">Categoryname</label>
	<input id="categoryname" name="categoryname" type="text" maxlength="255"  value="<?php echo $rows->categoryname;?>" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="category_show">Category Show</label>
	<select name="category_show" id="category_show" class="required form-control  choz">
	<option value=""></option>
	<option <?php echo ($rows->category_show == "cat1") ? 'selected' : '' ;?> value="cat1">cat1</option>
	<option <?php echo ($rows->category_show == "cat2") ? 'selected' : '' ;?> value="cat2">cat2</option>
	<option <?php echo ($rows->category_show == "cat3") ? 'selected' : '' ;?> value="cat3">cat3</option>
	<option <?php echo ($rows->category_show == "cat4") ? 'selected' : '' ;?> value="cat4">cat4</option>
	<option <?php echo ($rows->category_show == "cat5") ? 'selected' : '' ;?> value="cat5">cat5</option>
	<option <?php echo ($rows->category_show == "cat6") ? 'selected' : '' ;?> value="cat6">cat6</option>
	<option <?php echo ($rows->category_show == "cat7") ? 'selected' : '' ;?> value="cat7">cat7</option>
	</select>
	</div>

	 <div class="controls">
	 <div class="col-md-2" style="padding:0;">
	 <input type="submit" name="button" id="hButton" class="btn btn-primary btn-lg" value="<?php echo LANG_UPDATE_RECORD;?>" />
	 </div>
	 <div class="col-md-3" style="padding:0;">
	 <div id="output"></div>
	 </div>
	 </div>
	  
	</form>
	</p>
	 
	
	</div>
 </div><!--/col-12-->
 </div><!--/heze-table-->
	