
	<?php
	/*
	* =======================================================================
	* FILE NAME:        setting_web.php
	* DATE CREATED:  	01-06-2015
	* FOR TABLE:  		setting_web
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-lg-12">	
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=setting_web&do=add" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_ADD;?>"><i class="fa fa-plus"></i> <?php echo LANG_ADD;?></a>
	<a href="<?php echo H_ADMIN_MAIN;?>&view=setting_web&do=export&hexport=yes&etype=printer" target="_blank" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_PRINT;?>"><i class="fa fa-print"></i> <?php echo LANG_PRINT;?></a>
	<a href="<?php echo H_ADMIN_MAIN;?>&view=setting_web&do=export&hexport=yes&etype=excel" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_EXCEL;?>"><i class="fa fa-table"></i> <?php echo LANG_EXCEL;?></a>
	<a href="<?php echo H_ADMIN_MAIN;?>&view=setting_web&do=export&hexport=yes&etype=word" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_WORD;?>"><i class="fa fa-file-o"></i> <?php echo LANG_WORD;?></a>
	</ul>
	
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> Setting Web</h3></div>
  <div class="panel-body">
   <p>
	<input id="filter" class="input-sm" type="text" placeholder="<?php echo LANG_QUICK_SEARCH;?>"/>
	</p>
  </div>
	
	<table class="table table-bordered table-hover table-striped" data-filter="#filter" data-page-size="<?php echo RECORD_PER_PAGE;?>" data-page-previous-text="<?php echo LANG_PREVIOUS;?>" data-page-next-text="<?php echo LANG_NEXT;?>">
	<thead>
    <tr>
      <th>Name Web</th>
	  <th data-hide="phone,tablet">Decription</th>
	  <th><?php echo LANG_ACTIONS;?></th>
	</tr>
  </thead>
  <tbody>
  
   <?php
	foreach($result as $rows)
			{
	?>
	<tr>
	<td><?php echo $rows->name_web;?></td>
	<td><?php echo $rows->decription;?></td>
	<td class="table-actions">
	 <div class="btn-group">
	 <a href="<?php echo H_ADMIN;?>&view=setting_web&id=<?php echo $rows->id;?>&do=details"  class="btn btn-info btn-xs"><span class="fa fa-search-plus tip" title="<?php echo LANG_TIP_DETAILS;?>"></span></a>
	<a href="<?php echo H_ADMIN;?>&view=setting_web&id=<?php echo $rows->id;?>&do=update" class="btn btn-primary btn-xs"><span class="fa fa-edit tip" title="<?php echo LANG_TIP_UPDATE;?>"></span></a>
	 <a href="<?php echo H_ADMIN;?>&view=setting_web&id=<?php echo $rows->id;?>&do=delete&dfile=<?php echo $rows->logo_web;?>" class="btn btn-danger btn-xs" data-confirm="<?php echo LANG_DELETE_AUTH;?>"> <span class="fa fa-times tip" title="<?php echo LANG_TIP_DELETE;?>"></span></a>
	 </div>
	 </td>
    </tr>
	<?php }?>
  </tbody>
</table>
<div class="text-right"><ul class="pagination"><?php echo $paging;?></ul></div>
</div><br />
 </div><!--/col-12-->
 </div><!--/heze-table-->