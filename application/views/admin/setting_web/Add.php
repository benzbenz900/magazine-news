
	<?php
	/*
	* =======================================================================
	* FILE NAME:        setting_web.php
	* DATE CREATED:  	01-06-2015
	* FOR TABLE:  		setting_web
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-12">
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=setting_web&do=viewall" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_VIEWALL;?>"><i class="fa fa-reply"></i> <?php echo LANG_GO_BACK;?></a>
	</ul>
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> <?php echo LANG_CREATE_NEW;?> Setting Web</h3></div>
  <div class="panel-body pformmargin">
	
	 
	 <p>
	 <form action="<?php htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" name="hezecomform" id="hezecomform" enctype="multipart/form-data">
	
	<?php if(isset($errors))form_errors($errors);?>
	
	<div class="form-group">
    <label class="control-label" for="name_web">Name Web</label>
	<input id="name_web" name="name_web" type="text" maxlength="255"  value="" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="decription">Decription</label>
	<input id="decription" name="decription" type="text" maxlength="255"  value="" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="keyword">Keyword</label>
	<input id="keyword" name="keyword" type="text" maxlength="255"  value="" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="logo_web">Logo Web</label>
	
	<input id="logo_web" name="logo_web"type="file" class="form-control styler"/>
	
	</div>

	<div class="form-group">
    <label class="control-label" for="open_web">Open Web</label>
	<select name="open_web" id="open_web" class="required form-control  choz">
	<option value=""></option><option value="Open">Open</option>
	<option value="Close">Close</option>
	</select>
	</div>

	<div class="form-group">
    <label class="control-label" for="contact_us">About US</label>
	
		<textarea rows="5" id="contact_us" name="contact_us" class="form-control " /></textarea>
	<?php HezecomEditor('contact_us'); ?></div>

	 <div class="controls">
	 <div class="col-md-2" style="padding:0;">
	 <input type="submit" name="button" id="hButton" class="btn btn-primary btn-lg" value="<?php echo LANG_CREATE_RECORD;?>" />
	 </div>
	 <div class="col-md-3" style="padding:0;">
	 <div id="output"></div>
	 </div>
	 </div>
	  
	</form>
	</p>
	 
	
	</div>
 </div><!--/col-12-->
 </div><!--/heze-table-->
	