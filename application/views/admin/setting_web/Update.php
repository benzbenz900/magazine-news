
	<?php
	/*
	* =======================================================================
	* FILE NAME:        setting_web.php
	* DATE CREATED:  	01-06-2015
	* FOR TABLE:  		setting_web
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-12">
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=setting_web&id=<?php echo $rows->id;?>&do=details" title="View Details" class="btn btn-default btn-sm tip"><i class="fa fa-th-list"></i> <?php echo LANG_DETAILS;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=setting_web&id=<?php echo $rows->id;?>&do=delete&dfile=<?php echo $rows->logo_web;?>" title="<?php echo LANG_TIP_DELETE;?>" class="btn btn-default btn-sm tip" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><i class="fa fa-trash-o"></i> <?php echo LANG_DELETE;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=setting_web&do=viewall" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_VIEWALL;?>"><i class="fa fa-reply"></i> <?php echo LANG_GO_BACK;?></a>
	</ul>
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> <?php echo LANG_UPDATE;?> Setting Web</h3></div>
  <div class="panel-body pformmargin">
	
	 
	 <p>
	 <form action="<?php htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" name="hezecomform" id="hezecomform" enctype="multipart/form-data">
	
	<?php if(isset($errors))form_errors($errors);?>
	
	<input type="hidden" name="id" value="<?php echo $rows->id;?>">
	<div class="form-group">
    <label class="control-label" for="name_web">Name Web</label>
	<input id="name_web" name="name_web" type="text" maxlength="255"  value="<?php echo $rows->name_web;?>" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="decription">Decription</label>
	<input id="decription" name="decription" type="text" maxlength="255"  value="<?php echo $rows->decription;?>" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="keyword">Keyword</label>
	<input id="keyword" name="keyword" type="text" maxlength="255"  value="<?php echo $rows->keyword;?>" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="logo_web">Logo Web</label>
	<input id="logo_web" name="logo_web"type="file" class="styler"/><br><?php if(is_file(UPLOAD_FOLDER.$rows->logo_web)){?><a href="#"><img src="<?php echo THUMB_FOLDER.$rows->logo_web;?>"></a><br><?php }?>
	<?php if(is_file(UPLOAD_FOLDER.$rows->logo_web)){?>
	<a href="<?php echo H_ADMIN;?>&view=setting_web&id=<?php echo $rows->id;?>&dfile=<?php echo $rows->logo_web;?>&do=delete&fdel=file" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><span class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> <?php echo LANG_DELETE;?></span></a><br><?php }?>
	
	</div>

	<div class="form-group">
    <label class="control-label" for="open_web">Open Web</label>
	<select name="open_web" id="open_web" class="required form-control  choz">
	<option value="<?php echo $rows->open_web;?>"><?php echo $rows->open_web;?></option><option value="Open">Open</option>
	<option value="Close">Close</option>
	</select>
	</div>

	<div class="form-group">
    <label class="control-label" for="contact_us">About US</label>
	
		<textarea rows="5" id="contact_us" name="contact_us" class="form-control " /><?php echo $rows->contact_us;?></textarea>
	<?php HezecomEditor('contact_us'); ?></div>

	 <div class="controls">
	 <div class="col-md-2" style="padding:0;">
	 <input type="submit" name="button" id="hButton" class="btn btn-primary btn-lg" value="<?php echo LANG_UPDATE_RECORD;?>" />
	 </div>
	 <div class="col-md-3" style="padding:0;">
	 <div id="output"></div>
	 </div>
	 </div>
	  
	</form>
	</p>
	 
	
	</div>
 </div><!--/col-12-->
 </div><!--/heze-table-->
	