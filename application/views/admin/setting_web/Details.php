
	<?php
	/*
	* =======================================================================
	* FILE NAME:        setting_web.php
	* DATE CREATED:  	01-06-2015
	* FOR TABLE:  		setting_web
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-lg-12">
	
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=setting_web&do=viewall" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_VIEWALL;?>"><i class="fa fa-reply"></i> <?php echo LANG_GO_BACK;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=setting_web&do=add" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_ADD;?>"><i class="fa fa-plus"></i> <?php echo LANG_ADD;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=setting_web&id=<?php echo $rows->id;?>&do=update" title="<?php echo LANG_TIP_UPDATE;?> Record" class="btn btn-default btn-sm tip"><i class="fa fa-edit"></i> <?php echo LANG_UPDATE;?></a>
	
	<a href="<?php echo H_ADMIN_MAIN;?>&view=setting_web&id=<?php echo $rows->id;?>&do=export2&hexport=yes&etype=word" title="<?php echo LANG_TIP_WORD;?>" class="btn btn-default btn-sm tip"><i class="fa fa-file-o"></i> <?php echo LANG_WORD;?></a>
	
	<a href="<?php echo H_ADMIN_MAIN;?>&view=setting_web&id=<?php echo $rows->id;?>&do=export2&hexport=yes&etype=printer" title="<?php echo LANG_TIP_PRINT;?>" target="_blank" class="btn btn-default btn-sm tip"><i class="fa fa-print"></i> <?php echo LANG_PRINT;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=setting_web&id=<?php echo $rows->id;?>&do=delete&dfile=" title="<?php echo LANG_TIP_DELETE_ALL;?>" class="btn btn-default btn-sm tip" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><i class="fa fa-trash-o"></i> <?php echo LANG_DELETE;?></a>
	</ul>
	
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> Setting Web <?php echo LANG_DETAILS;?></h3></div>
	<table class="table table-striped table-bordered" data-page-size="200">
	 <tbody>
		  	
	<tr>
	<th>Name Web</th><td><?php echo $rows->name_web;?></td>
	</tr>
		
	<tr>
	<th>Decription</th><td><?php echo $rows->decription;?></td>
	</tr>
		
	<tr>
	<th>Keyword</th><td><?php echo $rows->keyword;?></td>
	</tr>
		
	<tr>
	<th>Logo Web</th><td class='gallery'><?php if(is_file(UPLOAD_FOLDER.$rows->logo_web)){?><a href='<?php echo UPLOAD_FOLDER.$rows->logo_web;?>' data-rel='hezebox'><img src='<?php echo THUMB_FOLDER.$rows->logo_web;?>'></a><?php }?></td>
	</tr>
		
	<tr>
	<th>Open Web</th><td><?php echo $rows->open_web;?></td>
	</tr>
		
	<tr>
	<th>Contact Us</th><td><?php echo $rows->contact_us;?></td>
	</tr>
	</tbody>
	</table>
	</div>
 </div><!--/col-12-->
 </div><!--/heze-table-->
	