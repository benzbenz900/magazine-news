
	<?php
	/*
	* =======================================================================
	* FILE NAME:        ad_manager.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		ad_manager
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-12">
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=ad_manager&do=viewall" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_VIEWALL;?>"><i class="fa fa-reply"></i> <?php echo LANG_GO_BACK;?></a>
	</ul>
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> <?php echo LANG_CREATE_NEW;?> Ad Manager</h3></div>
  <div class="panel-body pformmargin">
	
	 
	 <p>
	 <form action="<?php htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" name="hezecomform" id="hezecomform" enctype="multipart/form-data">
	
	<?php if(isset($errors))form_errors($errors);?>
	
	<div class="form-group">
    <label class="control-label" for="name_ad">Name Ad</label>
	<input id="name_ad" name="name_ad" type="text" maxlength="255"  value="" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="codelink">Codelink</label>
	
		<textarea rows="5" id="codelink" name="codelink" class="form-control " /></textarea>
	<?php HezecomEditor('codelink'); ?></div>

	<div class="form-group">
    <label class="control-label" for="image">Image</label>
	
	<input id="image" name="image"type="file" class="form-control styler"/>
	
	</div>

	<div class="form-group">
    <label class="control-label" for="show_lp">Show Lp</label>
	<select name="show_lp" id="show_lp" class="required form-control  choz">
	<option value=""></option><option value="Show">Show</option>
	<option value="Not Show">Not Show</option>
	</select>
	</div>

	<div class="form-group">
    <label class="control-label" for="end_date">End Date</label>
	<input name="end_date" class="datepicker form-control " type="text" maxlength="535" value="<?php echo date('Y-m-d');?>" required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="position">Position</label>
	<select name="position" id="position" class="required form-control  choz">
	<option value=""></option><option value="AD_CODE728">AD_CODE728</option>
	<option value="AD_CODE350">AD_CODE350</option>
	<option value="AD_CODE468">AD_CODE468</option>
	<option value="AD_CODEPOST">AD_CODEPOST</option>
	</select>
	</div>

	 <div class="controls">
	 <div class="col-md-2" style="padding:0;">
	 <input type="submit" name="button" id="hButton" class="btn btn-primary btn-lg" value="<?php echo LANG_CREATE_RECORD;?>" />
	 </div>
	 <div class="col-md-3" style="padding:0;">
	 <div id="output"></div>
	 </div>
	 </div>
	  
	</form>
	</p>
	 
	
	</div>
 </div><!--/col-12-->
 </div><!--/heze-table-->
	