
	<?php
	/*
	* =======================================================================
	* FILE NAME:        ad_manager.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		ad_manager
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-lg-12">
	
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=ad_manager&do=viewall" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_VIEWALL;?>"><i class="fa fa-reply"></i> <?php echo LANG_GO_BACK;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=ad_manager&do=add" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_ADD;?>"><i class="fa fa-plus"></i> <?php echo LANG_ADD;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=ad_manager&id=<?php echo $rows->id;?>&do=update" title="<?php echo LANG_TIP_UPDATE;?> Record" class="btn btn-default btn-sm tip"><i class="fa fa-edit"></i> <?php echo LANG_UPDATE;?></a>
	
	<a href="<?php echo H_ADMIN_MAIN;?>&view=ad_manager&id=<?php echo $rows->id;?>&do=export2&hexport=yes&etype=word" title="<?php echo LANG_TIP_WORD;?>" class="btn btn-default btn-sm tip"><i class="fa fa-file-o"></i> <?php echo LANG_WORD;?></a>
	
	<a href="<?php echo H_ADMIN_MAIN;?>&view=ad_manager&id=<?php echo $rows->id;?>&do=export2&hexport=yes&etype=printer" title="<?php echo LANG_TIP_PRINT;?>" target="_blank" class="btn btn-default btn-sm tip"><i class="fa fa-print"></i> <?php echo LANG_PRINT;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=ad_manager&id=<?php echo $rows->id;?>&do=delete&dfile=" title="<?php echo LANG_TIP_DELETE_ALL;?>" class="btn btn-default btn-sm tip" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><i class="fa fa-trash-o"></i> <?php echo LANG_DELETE;?></a>
	</ul>
	
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> Ad Manager <?php echo LANG_DETAILS;?></h3></div>
	<table class="table table-striped table-bordered" data-page-size="200">
	 <tbody>
		  	
	<tr>
	<th>Name Ad</th><td><?php echo $rows->name_ad;?></td>
	</tr>
		
	<tr>
	<th>Codelink</th><td><?php echo $rows->codelink;?></td>
	</tr>
		
	<tr>
	<th>Image</th><td class='gallery'><?php if(is_file(UPLOAD_FOLDER.$rows->image)){?><a href='<?php echo UPLOAD_FOLDER.$rows->image;?>' data-rel='hezebox'><img src='<?php echo THUMB_FOLDER.$rows->image;?>'></a><?php }?></td>
	</tr>
		
	<tr>
	<th>Show Lp</th><td><?php echo $rows->show_lp;?></td>
	</tr>
		
	<tr>
	<th>End Date</th><td><?php echo $rows->end_date;?></td>
	</tr>
		
	<tr>
	<th>Position</th><td><?php echo $rows->position;?></td>
	</tr>
	</tbody>
	</table>
	</div>
 </div><!--/col-12-->
 </div><!--/heze-table-->
	