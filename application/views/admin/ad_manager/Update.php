
	<?php
	/*
	* =======================================================================
	* FILE NAME:        ad_manager.php
	* DATE CREATED:  	02-06-2015
	* FOR TABLE:  		ad_manager
	* PRODUCED BY:		lnwPHP Thailand (lnwPHP Admin Manager)
	* AUTHOR:			Benz@lnwphp (https://www.lnwphp.in.th) benzbenz900@gmail.com
	* =======================================================================
	*/
	if(!defined('VALID_DIR')) die('You are not allowed to execute this file directly');
	?>
	
	<div class="heze-table">
	<div class="col-12">
	<ul class="nav nav-tabs pull-right">
	<a href="<?php echo H_ADMIN;?>&view=ad_manager&id=<?php echo $rows->id;?>&do=details" title="View Details" class="btn btn-default btn-sm tip"><i class="fa fa-th-list"></i> <?php echo LANG_DETAILS;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=ad_manager&id=<?php echo $rows->id;?>&do=delete&dfile=<?php echo $rows->image;?>" title="<?php echo LANG_TIP_DELETE;?>" class="btn btn-default btn-sm tip" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><i class="fa fa-trash-o"></i> <?php echo LANG_DELETE;?></a>
	
	<a href="<?php echo H_ADMIN;?>&view=ad_manager&do=viewall" class="btn btn-default btn-sm tip" title="<?php echo LANG_TIP_VIEWALL;?>"><i class="fa fa-reply"></i> <?php echo LANG_GO_BACK;?></a>
	</ul>
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> <?php echo LANG_UPDATE;?> Ad Manager</h3></div>
  <div class="panel-body pformmargin">
	
	 
	 <p>
	 <form action="<?php htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" name="hezecomform" id="hezecomform" enctype="multipart/form-data">
	
	<?php if(isset($errors))form_errors($errors);?>
	
	<input type="hidden" name="id" value="<?php echo $rows->id;?>">
	<div class="form-group">
    <label class="control-label" for="name_ad">Name Ad</label>
	<input id="name_ad" name="name_ad" type="text" maxlength="255"  value="<?php echo $rows->name_ad;?>" class="form-control " required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="codelink">Codelink</label>
	
		<textarea rows="5" id="codelink" name="codelink" class="form-control " /><?php echo $rows->codelink;?></textarea>
	<?php HezecomEditor('codelink'); ?></div>

	<div class="form-group">
    <label class="control-label" for="image">Image</label>
	<input id="image" name="image"type="file" class="styler"/><br><?php if(is_file(UPLOAD_FOLDER.$rows->image)){?><a href="#"><img src="<?php echo THUMB_FOLDER.$rows->image;?>"></a><br><?php }?>
	<?php if(is_file(UPLOAD_FOLDER.$rows->image)){?>
	<a href="<?php echo H_ADMIN;?>&view=ad_manager&id=<?php echo $rows->id;?>&dfile=<?php echo $rows->image;?>&do=delete&fdel=file" data-confirm="<?php echo LANG_DELETE_AUTH;?>"><span class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> <?php echo LANG_DELETE;?></span></a><br><?php }?>
	
	</div>

	<div class="form-group">
    <label class="control-label" for="show_lp">Show Lp</label>
	<select name="show_lp" id="show_lp" class="required form-control  choz">
	<option value="<?php echo $rows->show_lp;?>"><?php echo $rows->show_lp;?></option><option value="Show">Show</option>
	<option value="Not Show">Not Show</option>
	</select>
	</div>

	<div class="form-group">
    <label class="control-label" for="end_date">End Date</label>
	<input name="end_date" class="datepicker form-control " type="text" maxlength="535" value="<?php echo $rows->end_date;?>" required/>
	</div>

	<div class="form-group">
    <label class="control-label" for="position">Position</label>
	<select name="position" id="position" class="required form-control  choz">
	<option value="<?php echo $rows->position;?>"><?php echo $rows->position;?></option><option value="AD_CODE728">AD_CODE728</option>
	<option value="AD_CODE350">AD_CODE350</option>
	<option value="AD_CODE468">AD_CODE468</option>
	<option value="AD_CODEPOST">AD_CODEPOST</option>
	</select>
	</div>

	 <div class="controls">
	 <div class="col-md-2" style="padding:0;">
	 <input type="submit" name="button" id="hButton" class="btn btn-primary btn-lg" value="<?php echo LANG_UPDATE_RECORD;?>" />
	 </div>
	 <div class="col-md-3" style="padding:0;">
	 <div id="output"></div>
	 </div>
	 </div>
	  
	</form>
	</p>
	 
	
	</div>
 </div><!--/col-12-->
 </div><!--/heze-table-->
	