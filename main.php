<?php
		//For Handling Ajax Call and Exports
		ob_start();
		session_start();
		include('config/config.php');
		include('language/eng.php');
		include('libraries/functions.php');
		include_once('libraries/class_dbcon.php');
		include_once('libraries/upload_class.php');
	
		if(get('view') and get('pg')=='admin'){
		if(!isset($_SESSION[H_USER_SESSION])){
		include('libraries/views/admin/login.php');
		}
		include(APP_FOLDER.'/controllers/admin/main.php');
		}
		else{
		include(APP_FOLDER.'/controllers/public/main.php');
		}
		ob_end_flush();
	
?>