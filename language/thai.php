<?php 
	//success msgs
	define('LANG_SUCCESS_ADD','เพิ่มข้อมูลสำเร็จ');
	define('LANG_SUCCESS_UPDATE','ปรับปรุงข้อมูลเียบร้อย');
	define('LANG_SUCCESS_DELETE','ลบข้อมูลเรียบร้อย');
	define('LANG_ERROR_MSG','เกิดข้อผิดพลาดขึ้น');
	define('LANG_BACKUP_RESTORED','การสำรองฐานข้อมูลได้รับกลับคืนเรียบร้อยแล้ว');
	define('LANG_BACKUP_DELETED','การสำรองฐานข้อมูลถูกลบประสบความสำเร็จ');
	define('LANG_BACKUP_CREATED','การสำรองฐานข้อมูลสร้างเรียบร้อย');
	define('LANG_DATABASE_DELETE_AUTH','คุณแน่ใจหรือว่าต้องการลบสำรองข้อมูลนี้');
	define('LANG_DELETE_AUTH','Are you sure you want to delete this Record(s)');
	define('LANG_CSV_SUCCESS','CSV data successfully imported to database!!');
	define('LANG_SUCCESS_TRUNCATE','Table truncate was successfully');
	define('LANG_TRUNCATE_DELETE_AUTH','Are you sure you want to CLEAR all the records in this TABLE?');
	
	//Reports
	define('LANG_REPORT_TITLE','Hezecom Code Generator Ultimate');
	define('LANG_REPORT_SUB_TITLE','Database Report as at <em>'.date('d/m/Y').'</em>');
	define('LANG_REPORT_TABLE','Table Name:');
	
	//Tables
	define('LANG_DOWNLOAD','Download');
	define('LANG_RESTORE','Restore');
	define('LANG_DELETE','Delete');
	define('LANG_UPDATE','Update');
	define('LANG_ADD','Add');
	define('LANG_DETAILS','Details');
	define('LANG_PRINT','Print');
	define('LANG_EXCEL','Excel');
	define('LANG_PDF','PDF');
	define('LANG_CSV','CSV');
	define('LANG_CSV_EXPORT','Export CSV');
	define('LANG_CSV_IMPORT','Import CSV');
	define('LANG_WORD','Word');
	define('LANG_GO_BACK','Go Back');
	define('LANG_CREATE','Create');
	define('LANG_CREATE_NEW','Create New');
	define('LANG_REMOVE','Remove');
	define('LANG_TRUNCATE','Empty');
	define('LANG_UPDATE_THIS_RECORD','Update this Record');
	define('LANG_ADD_NEW_RECORD','Add New Record');
	define('LANG_ACTIONS','Actions');
	define('LANG_PREVIEW','Preview');
	//Paging
	define('LANG_NEXT','Next');
	define('LANG_PREVIOUS','Prev');
	define('LANG_FIRST','First');
	define('LANG_LAST','Last');
	define('LANG_PAGE','Page');
	define('LANG_ITEM_PER_PAGE','Record per page');
	//Login
	define('LANG_LOGOUT','Logout');
	define('LANG_LOGIN_IN_AS','You are logged in as');
	define('LANG_ADMIN_LOGIN','Admin Login');
	define('LANG_ADMIN_ERROR','Invalid Username or Password');
	define('LANG_ADMIN_USERNAME','Enter your Username');
	define('LANG_ADMIN_PASSWORD','Enter your Password');
	define('LANG_ADMIN_BUTTON','Login to Admin');
	define('LANG_ADMIN_INVALID_USERNAME','Sorry that username doesn\'t exists.');
	define('LANG_ADMIN_INVALID_PASSWORD','Sorry, but we need your username and password.');
	define('LANG_ADMIN_INVALID_ACTIVATION','Sorry, but you need to activate your account.');
	//index
	define('LANG_DATABASE_MANAGER','Database Manager');
	define('LANG_DASHBOARD','Dashboard');
	//Forms
	define('LANG_ADD_FIELD','Add Field');
	define('LANG_UPDATE_RECORD','Update Record');
	define('LANG_CREATE_RECORD','Create Record');
	define('LANG_QUICK_SEARCH','Quick Search...');
	//Tooltips
	define('LANG_TIP_ADD','Add New Record');
	define('LANG_TIP_PRINT','Print Records');
	define('LANG_TIP_EXCEL','Export to Excel');
	define('LANG_TIP_WORD','Export to Word');
	define('LANG_TIP_PDF','Export to PDF');
	define('LANG_TIP_CSV','CSV');
	define('LANG_TIP_CSV_EXPORT','Export to CSV');
	define('LANG_TIP_CSV_IMPORT','Import to CSV');
	define('LANG_TIP_TRUNCATE','Empty Table');
	define('LANG_TIP_VIEWALL','View All Records');
	define('LANG_TIP_UPDATE','Update');
	define('LANG_TIP_DELETE','Delete');
	define('LANG_TIP_DELETE_ALL','Delete Entire Record');
	define('LANG_TIP_DETAILS','View Details');
	//Backup page
	define('LANG_UPLOAD_CSV_TO_TABLE','Upload CSV to Table');
	define('LANG_CSV_TO','Where are you uploading to:');
	define('LANG_CSV_SELECT_FILE','Select the CSV File');
	define('LANG_UPLOAD','Upload');
	define('LANG_BACKUP_MANAGEMENT','Backup Management');
	define('LANG_CREATE_BACKUP','Create Backup');
	define('LANG_SYSTEM_USERS','System Users');

?>