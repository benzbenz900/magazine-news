<?php
	session_start();
	include('config/config.php');
	include('language/thai.php');
	include('libraries/functions.php');
	include_once('libraries/class_dbcon.php');
	include_once('libraries/upload_class.php');
	include_once('libraries/system_users.php');
	$haccess = new admin_users_model();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo H_TITLE;?></title>
<link rel="stylesheet" type="text/css" href="<?php echo H_THEME;?>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo H_THEME;?>/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo H_THEME;?>/css/footable.core.min.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo H_THEME;?>/css/jquery.lightbox.min.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="<?php echo H_THEME;?>/css/datepicker.css">
<link href="<?php echo H_THEME;?>/css/admin.css" rel="stylesheet">
<link href="<?php echo H_THEME;?>/css/chosen.min.css" rel="stylesheet">
<script src="<?php echo H_THEME;?>/js/modernizr.custom.js"></script>
<script src="//cdn.ckeditor.com/4.5.1/full/ckeditor.js"></script>
</head>
<body>
  		<?php
  		if(get('msg')=='add'){success_msg(LANG_SUCCESS_ADD);}
		elseif(get('msg')=='update'){success_msg(LANG_SUCCESS_UPDATE);}
		elseif(get('msg')=='delete'){success_msg(LANG_SUCCESS_DELETE);}
		elseif(get('msg')=='truncate'){success_msg(LANG_SUCCESS_TRUNCATE);}
		elseif(get('msg')=='error'){error_msg(LANG_ERROR_MSG);}
		elseif(get('msg')=='backup'){success_msg(LANG_BACKUP_CREATED);}
		elseif(get('dbrestore')!=''){success_msg(LANG_BACKUP_RESTORED);}
		elseif(get('dbfile')!=''){success_msg(LANG_BACKUP_DELETED);}
		elseif(get('msg')=='importer'){success_msg(LANG_CSV_SUCCESS);}
		?>
    <div id="hezewraper">
      <div class="navbar navbar-static-top navbar-fixed-top navbar-inverse">
          <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" id="showLeft" style="border:0;">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
      <a class="navbar-brand" style="color:#FFF;" href="index.php"><img style="height: 30px;" alt="lnwPHP Logo" src="https://www.lnwphp.in.th/file/2015/02/11/logo_w_2.png"> <?php echo H_TITLE;?></a>
          <div class="collapse navbar-collapse navbar-ex1-collapse">
          <?php  if(isset($_SESSION[H_USER_SESSION])){?>
            <p class="navbar-text pull-right" style="color:#FFF;">
              <?php echo LANG_LOGIN_IN_AS.'  '.$_SESSION[H_USER_SESSION];?>  <a href="<?php echo H_ADMIN;?>&view=hsys_users&do=logout" class="btn btn-info btn-sm"> <i class=" fa fa-power-off"></i> <?php echo LANG_LOGOUT;?></a> 
              <a href="<?php echo H_ADMIN;?>&view=hsys_users2&do=details" class="btn btn-danger btn-sm"> <i class=" fa fa-lock"></i> Account</a>
            </p>
            <?php }?>
          </div><!--/.nav-collapse -->
       
      </div>
      
     <?php  if(isset($_SESSION[H_USER_SESSION])){?> 
    <!--MOBILE NAV-->  
      <div class="phone-navigation">
      <div class="heze-mobilemenu-push">
		<nav class="heze-mobilemenu heze-mobilemenu-vertical heze-mobilemenu-left" id="heze-mobilemenu-s1">
        <div class="divscroll">
			<ul class="heze-accordion">
			<li id="one" class="files">
		<a href="#1" class="active"><i class="fa fa-dashboard aln"></i> <?php echo LANG_DASHBOARD;?><span><i class="fa fa-chevron-down"></i></span></a>
		<ul class="sub-menu" style="display: block;">
        <?php if($haccess->UserAccess()->user_position==1){?>
        <li><a href="<?php echo H_ADMIN;?>&view=hsys_users&do=viewall"><i class="fa fa-users"></i><?php echo LANG_SYSTEM_USERS;?></a></li>
         <li><a href="<?php echo H_ADMIN;?>&view=hsys_users2&do=details"> <i class=" fa fa-lock"></i> Account</a></li>
		<li><a href="<?php echo H_ADMIN;?>&view=hsys_users&do=logout"><i class="fa fa-power-off"></i><?php echo LANG_LOGOUT;?></a></li>
		<?php }?>
        <?php include(APP_FOLDER.'/views/admin/menu.php');?>
				</ul>
                </li>
           
           </ul>
           </div>
		</nav>
        </div>
        </div>
      <!--/MOBILE NAV-->

     <!-- SIDE MENU -->
     
    <div class="heze-navmenu">
    <div class="divscroll">
			<ul class="heze-accordion">
			<li id="one" class="files">
				<a href="#1" class="active"><i class="fa fa-dashboard aln"></i> <?php echo LANG_DASHBOARD;?><span><i class="fa fa-chevron-down"></i></span></a>
				<ul class="sub-menu" style="display: block;">
                <?php if($haccess->UserAccess()->user_position==1){?>
        <li><a href="<?php echo H_ADMIN;?>&view=hsys_users&do=viewall"><i class="fa fa-users"></i><?php echo LANG_SYSTEM_USERS;?></a></li>
        <?php }?>
                <?php include(APP_FOLDER.'/views/admin/menu.php');?>
				</ul>
                </li>
                
           </ul>
           </div>
</div>
<!-- END SIDE MENU -->
<?php }?>
<!-- BODY CONTENTS -->
<div class="heze-container">
<h4><?php echo H_TITLE;?> </h4>
<?php  if(isset($_SESSION[H_USER_SESSION])){?>
<div class="headbread">
 <div class="col-6">
  <h3><i class="fa fa-dashboard"></i> <?php echo LANG_DASHBOARD;?></h3>
  </div>
</div> 
  <?php }?>
  <!--TABLE-->
  <?php 
		$haccess->logged_in_protect();
		if(get('view')){
		include(APP_FOLDER.'/controllers/admin/main.php');
		include('libraries/controllers/system_users.php');
		}else{
		?>
     
 <div class="col-lg-12">
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reorder"></i> Welcome!</h3></div>
  <div class="panel-body" style="padding:10px;">
   <p>
   ยินดีต้อนรับสู่ระบบจัดการของเรา
   <h3>Pomotion ของ lnwPHP.in.th</h3>
   <span id="show_pomotion"></span>
   </p>
   <script type="text/javascript">
   function load_pomotion()
{
    var xmlhttp;
if (window.XMLHttpRequest){
  xmlhttp=new XMLHttpRequest();
  }else{
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("show_pomotion").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","https://www.lnwphp.in.th/pomotion.php",true);
xmlhttp.send();
}
window.load_pomotion();
   </script>
  </div>

</div>
 </div><!--/col-12-->
 <?php }?>
      
 </div><!--/heze-container-->       
</div><!--hezewraper-->
  
 
 <div id="footer" class="navbar-fixed-bottom">
      <div class="container">
        <p class="text-muted credit">&copy; <?php echo date('Y')?> <a href="https://www.lnwphp.in.th">lnwPHP Thailand</a></p>
      </div>
    </div>
<script src='<?php echo H_THEME;?>/js/jquery.js'></script>
<script src="<?php echo H_THEME;?>/js/bootstrap.min.js"></script>
<script src="<?php echo H_THEME;?>/js/footable.js" type="text/javascript"></script>
<script src="<?php echo H_THEME;?>/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo H_THEME;?>/js/jquery.lightbox.min.js" type="text/javascript"></script>
<script src="<?php echo H_THEME;?>/js/bootstrap-datepicker.js"></script>
<script src="<?php echo H_THEME;?>/js/jquery.form.js"></script>
<script src="<?php echo H_THEME;?>/js/chosen.jquery.min.js"></script>
<script src="<?php echo H_THEME;?>/js/hezecom.custom.js"></script>
  </body>
  </html>
